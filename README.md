# Order of the Bee Development Kit

This project is a Maven aggregator for the components of the *proposed* Order of the Bee Development Kit.  The proposed abbreviation for this kit is the **BeeDK**.

## Purpose

The sole purpose of this project is to build all the submodule components.  It is not the Maven Parent of any Maven project.  This means this project is not to be directly used by a developer using the BeeDK, but only a developer that is developing, building, or deploying the BeeDK.

## For BeeDK Users

### Configuration

There is no extra configuration required to use the BeeDK or its modules.  Some modules may require tools like Docker, but those are requirements of the module, not the BeeDK.

The BeeDK official artifacts are all deployed to Maven Central.  There are no 3rd party repositories needed.  You may develop and deploy your own extensions in other repositories though.  In those cases, the repositories will need to be defined as `pluginRepositories` in order for Maven Tiles to use them during its discovery.

### Modules

The following tiles (side-loaded components) are useful to users of the BeeDK.  BeeDK makes extensive use of [Maven Tiles](https://github.com/repaint-io/maven-tiles).

#### Public API

| Component                             | Folder Link   | Details |
| ------------------------------------- | ------------- | ------- |
| `beedk-acs-platform-module-tile`      | [Source](/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-module-tile)      | Intended to be inherited by all Alfresco Platform module projects; Includes ampification and more. |
| `beedk-acs-share-module-tile`         | [Source](/inteligr8/ootbee-beedk/src/stable/beedk-acs-share-module-tile)         | Intended to be inherited by all Alfresco Share module projects; Includes ampification, JS compression, and more. |
| `beedk-acs-platform-webapp-tile`      | [Source](/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-webapp-tile)      | Intended to be inherited by all Alfresco Platform web application projects; Includes installing AMPs, loading JARs, and more. |
| `beedk-acs-share-webapp-tile`         | [Source](/inteligr8/ootbee-beedk/src/stable/beedk-acs-share-webapp-tile)         | Intended to be inherited by all Alfresco Share web application projects; Includes installing AMPs, loading JARs, and more. |
| `beedk-ate-springboot`                | [Source](/inteligr8/ootbee-beedk/src/stable/beedk-ate-springboot) | Intended to be inherited by all Alfresco Transform Engine Spring Boot projects; Includes dependencies. |
| `beedk-ate-springboot-test`           | [Source](/inteligr8/ootbee-beedk/src/stable/beedk-ate-springboot-test) | Intended to be inherited by all Alfresco Transform Engine Spring Boot projects; Includes dependencies. |
| `beedk-ate-springboot-tile`           | [Source](/inteligr8/ootbee-beedk/src/stable/beedk-ate-springboot-tile) | Intended to be inherited by all Alfresco Transform Engine Spring Boot projects; Includes Spring Boot repackaging and more. |
| `beedk-activiti-ext-tile`             | *Coming Soon* | Intended to be inherited by all Alfresco Process Services or Activiti extension projects. |
| `beedk-flowable-ext-tile`             | *Coming Soon* | Intended to be inherited by all Flowable extension projects. |
| `beedk-camunda-ext-tile`              | *Coming Soon* | Intended to be inherited by all Camunda extension projects. |

#### ACS Platform Accessories

| Component                             | Folder Link   | Details |
| ------------------------------------- | ------------- | ------- |
| `beedk-acs-platform-docker-tile`      | *Coming Soon* | Intended to be inherited by any project that already includes a `beedk-acs-platform-webapp-tile` Maven Tile and wants to deliver a Docker image; Includes building and publishing the container images and more. |
| `beedk-acs-platform-self-it-tile`     | [Source](/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-self-it-tile) | Intended to be inherited by any ACS Platform module or web application Maven project.  It enables integration testing with the ACS Platform. |
| `beedk-acs-platform-sibling-it-tile`  | [Source](/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-sibling-it-tile) | Intended to be inherited by any Maven project that has a sibling ACS Platform module or web application Maven project.  It enables integration testing with the sibling ACS Platform.  This is great for ACS Share modules, ACS Share web applications, custom Alfresco Transform Engines, and APS extensions in **all-in-one** style Maven projects. |
| `beedk-acs-platform-artifact-it-tile` | [Source](/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-artifact-it-tile) | Intended to be inherited by any Maven project that does not have a sibling ACS Platform module or web application Maven project.  It enables integration testing with any ACS Platform as an already built Maven Artifact.  This is great for ACS Share modules, ACS Share web applications, custom Alfresco Transform Engines, APS extensions, and non-Alfresco applications in **standalone** projects. |
| `beedk-acs-search-it-tile`            | [Source](/inteligr8/ootbee-beedk/src/stable/beedk-acs-search-it-tile) | Intended to be inherited by any project that already includes a `beedk-acs-platform-*-it-tile` Maven Tile.  It enables integration testing of the ACS Platform with the Alfresco Search Services enabled.  This is great for any project requiring non-transactional search functionality for integration testing purposes. |
| `beedk-acs-lts-it-tile`               | [Source](/inteligr8/ootbee-beedk/src/stable/beedk-acs-lts-it-tile) | Intended to be inherited by any project that already includes a `beedk-acs-platform-*-it-tile` Maven Tile.  It enables integration testing of the ACS Platform with the Local Transform Service and the AIO Transform Engine enabled.  This is great for any project requiring basic transformation functionality for integration testing purposes. |
| `beedk-ats-it-tile`                   | [Source](/inteligr8/ootbee-beedk/src/stable/beedk-ats-it-tile) | Intended to be inherited by any project that already includes a `beedk-acs-platform-*-it-tile` Maven Tile.  It enables integration testing of the ACS Platform with the Alfresco Transform Service (ATS) enabled.  This is great for any project requiring Alfresco Enterprise transformation functionality for integration testing purposes. |
| `beedk-ate-it-tile`                   | [Source](/inteligr8/ootbee-beedk/src/stable/beedk-ate-it-tile) | Intended to be inherited by any project that already includes a `beedk-acs-platform-*-it-tile` Maven Tile.  It enables integration testing of the ACS Platform with a specific Alfresco Transform Engine (ATE) enabled.  This is great for any project requiring custom Alfresco transformation functionality for integration testing purposes. |

#### ACS Share Accessories

| Component                             | Folder Link   | Details |
| ------------------------------------- | ------------- | ------- |
| `beedk-acs-share-docker-tile`         | *Coming Soon* | Intended to be inherited by any project that already includes a `beedk-acs-share-webapp-tile` Maven Tile and wants to deliver a Docker image; Includes building and publishing the container images and more. |
| `beedk-acs-share-self-it-tile`        | [Source](/inteligr8/ootbee-beedk/src/stable/beedk-acs-share-self-it-tile) | Intended to be inherited by any ACS Share module or web application Maven project.  It enables integration testing with ACS Share. |

#### Alfresco Transform Engine Accessories

| Component                             | Folder Link   | Details |
| ------------------------------------- | ------------- | ------- |
| `beedk-ate-docker-tile`               | [Source](/inteligr8/ootbee-beedk/src/stable/beedk-ate-docker-tile) | Intended to be inherited by any project that already includes a `beedk-ate-springboot-tile` tile and wants to deliver a Docker image. |

#### Other Accessories

| Component                             | Folder Link   | Details |
| ------------------------------------- | ------------- | ------- |
| `beedk-springboot-docker-tile`        | [Source](/inteligr8/ootbee-beedk/src/stable/beedk-springboot-docker-tile) | Intended to be inherited by any Spring Boot project that wants to deliver a Docker image. |

#### Rapid Application Development

| Component                              | Folder Link   | Details |
| -------------------------------------- | ------------- | ------- |
| `beedk-acs-platform-self-rad-tile`     | [Source](/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-self-rad-tile) | Intended to be inherited by any Alfresco Platform module or web application projects; Includes startup with partial hot reloading. |
| `beedk-acs-platform-sibling-rad-tile`  | [Source](/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-sibling-rad-tile) | Intended to be inherited by any Maven project that has a sibling Alfresco Platform module or web application Maven project; provides ACS Platform capability to other RAD components. |
| `beedk-acs-platform-artifact-rad-tile` | [Source](/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-artifact-rad-tile) | Intended to be inherited by any Maven project that wants to use the Alfresco Platform web application; provides ACS Platform capability to other RAD components. |
| `beedk-acs-share-self-rad-tile`        | [Source](/inteligr8/ootbee-beedk/src/stable/beedk-acs-share-self-rad-tile) | Intended to be inherited by any Alfresco Platform module or web application projects; Includes startup with partial hot reloading. |
| `beedk-acs-search-rad-tile`            | [Source](/inteligr8/ootbee-beedk/src/stable/beedk-acs-search-rad-tile) | Intended to be inherited by any project that already includes a `beedk-acs-platform-*-rad-tile` Maven Tile.  It enables RAD of the ACS Platform with the Alfresco Search Services enabled.  This is great for any project requiring non-transactional search functionality. |
| `beedk-acs-lts-rad-tile`               | [Source](/inteligr8/ootbee-beedk/src/stable/beedk-acs-lts-rad-tile) | Intended to be inherited by any project that already includes a `beedk-acs-platform-*-rad-tile` Maven Tile.  It enables RAD of the ACS Platform with the Local Transform Service and the AIO Transform Engine enabled.  This is great for any project requiring basic transformation functionality. |
| `beedk-ats-rad-tile`                   | [Source](/inteligr8/ootbee-beedk/src/stable/beedk-ats-rad-tile) | Intended to be inherited by any project that already includes a `beedk-acs-platform-*-rad-tile` Maven Tile.  It enables RAD of the ACS Platform with the Alfresco Transform Service (ATS) enabled.  This is great for any project requiring Alfresco Enterprise transformation functionality. |
| `beedk-ate-rad-tile`                   | [Source](/inteligr8/ootbee-beedk/src/stable/beedk-ate-rad-tile) | Intended to be inherited by any project that already includes a `beedk-acs-platform-*-rad-tile` Maven Tile.  It enables RAD of the ACS Platform with a specific Alfresco Transform Engine (ATE) enabled.  This is great for any project requiring custom Alfresco transformation functionality. |

### Rapid Application Development

If your project includes RAD tiles, you can start the application with the following command.

```sh
mvn -Drad process-classes
```

To stop RAD and remove the Docker containers, you will need to do it through Docker commands.  This will differ between operating systems.  The Maven Archetypes provides scripts to assist with these operations called `rad.sh` or `rad.ps1`.

#### SH or BASH Shells

```sh
docker container stop `docker container ls -q --filter={artifactId}-*`
docker container rm `docker container ls -aq --filter={artifactId}-*`
```

#### Microsoft PowerShell

```PowerShell
docker container ls -q --filter={artifactId}-* | % { docker container stop $_ }
docker container ls -aq --filter={artifactId}-* | % { docker container rm $_ }
```

### Archetypes

| Archetype                             | Folder Link   | Details |
| ------------------------------------- | ------------- | ------- |
| `beedk-acs-platform-module-archetype` | [Source](/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-module-archetype) | Generates a standalone ACS Platform module project. |
| `beedk-acs-share-module-archetype`    | [Source](/inteligr8/ootbee-beedk/src/stable/beedk-acs-share-module-archetype) | Generates a standalone ACS Share module project. |
| `beedk-acs-allinone-archetype`        | [Source](/inteligr8/ootbee-beedk/src/stable/beedk-acs-allinone-archetype) | Generates a parent, ACS Platform webapp, ACS Share webapp, example ACS Platform module, example ACS Share module, and example ATE projects. |
| `beedk-ate-archetype`                 | [Source](/inteligr8/ootbee-beedk/src/stable/beedk-ate-archetype) | Generates a standalone Alfresco Transform Engine project. |
| `beedk-activiti-ext-archetype`        | *Coming Soon* | Generates a standalone APS/Activiti extension project. |
| `beedk-flowable-ext-archetype`        | *Coming Soon* | Generates a standalone [Flowable](https://flowable.com) extension project.  Flowable is a fork of Activiti 6. |
| `beedk-camunda-ext-archetype`         | *Coming Soon* | Generates a standalone [Camunda](https://camunda.com) extension project.  Camunda is a fork of Activiti 5. |
| `beedk-springboot-api-archetype`      | [Source](/inteligr8/ootbee-beedk/src/stable/beedk-springboot-api-archetype) | Generates a standalone Spring Boot application ready to interface with the ACS APIs (*APS Coming Soon*). |

You can use one of these Maven Archetypes to generate a new project by executing the following at the command line.

```sh
mvn archetype:generate
```

### Scaffolding

This is a feature that is designed to be extended in the near future.  It will include minimal engines on first release.  The goals is to "generate" resources and code for developers so they can quickly implement certain features.  Each feature supported is called a scaffolding engine.  The goal is to support several scaffold engines that would execute like the following command.

```sh
mvn -Dscaffold-webscript -Dbeedk.name=test-webscript -Dbeedk.method=get -Dbeedk.style=java
```

There is currently one scaffolding engine implemented, but it is useful across all the BeeDK Maven Tiles.

| Engine          | Options | Description |
| --------------- | ------- | ----------- |
| `scaffold`      |         | Generates the required files for the applicable Maven Tile. |

The plan is to implement the following engines (and more).

| Engine                  | Options | Description |
| ----------------------- | ------- | ----------- |
| `scaffold-webscript`    | `name`, `method` (**`get`**, `post`, ...), `style` (**`java`**, `java-ftl`, `js`) | Generates skeleton code for the configuration and implementation of a Spring Web Script; a ReST entrypoint. |
| `scaffold-behavior`     | `name`, `event` (`onUpdateProperties`, ...) | Generates skeleton code for the configuration and implementation of an Alfresco Policy Behavior; an event listener entrypoint. |
| `scaffold-job`          | `name`, `schedule` | Generates skeleton code for the configuration and implementation of a Quartz Job; a scheduled periodic execution entrypoint. |
| `scaffold-action`       | `name`  | Generates skeleton code for the configuration and implementation of an Alfresco Action; a function that may be executed through API calls or ACS Share hooks. |
| `scaffold-jsroot`       | `name`  | Generates skeleton code for the configuration and implementation of a JavaScript root object; a root scoped object accessible through JavaScript scripts. |
| `scaffold-rendition`    | `name`  | Generates skeleton code for the configuration of an Alfresco rendition. |
| `scaffold-evaluator`    | `name`, `type` (**`component`**, `em`) | Generates skeleton code for the configuration and implementation of a Spring Surf (ACS Share) evaluator; a conditional filter on user interface components or extensibility modules. |
| `scaffold-extmodule`    | `name`, `style` (**`config`**, `webscript`) | Generates skeleton code for the configuration and implementation of a Spring Surf extensibility module. |
| `scaffold-transformer`  | `name`  | Generates skeleton code for the configuration and implementation of an Alfresco Transform Engine Spring Boot application and transformer Spring service. |

## For BeeDK Maintainers

### Build Components

You can build the components of the BeeDK by cloning this repository and executing the typical Maven commands.  There is nothing special to consider.  A brief example is provided below.

```sh
git clone git@bitbucket.org:inteligr8/ootbee-beedk.git
cd ootbee-beedk
mvn clean package
```

### Development Model

This project uses a development model with two active Git branches: `develop` and `stable`.  All changes are to be made to branches not named `stable`.  If you use neither of these branches, first merge your changes into the `develop` branch.  Those changes should then be merged into the `stable` branch.  The following sets of commands intends to demonstrate all the Maven and Git commands used from inception to delivery of each bug fix or feature request.

#### Fork & Pull Requesst

If you choose to fork the repository, then no problem.  When you want to "push" you changes to this repository, create a pull request against the `develop` branch.

#### For Direct Maintainers

```sh
git clone git@bitbucket.org:inteligr8/ootbee-beedk.git
cd ootbee-beedk
git checkout develop
git checkout -b personal
# make your changes
mvn clean package
git add .
git commit -m "your informative commit message"
git push -u origin personal
# make more changes
mvn clean verify
git add .
git commit -m "another informative commmit message"
git push origin
# development done 
git checkout develop
git merge personal
# resolve conflicts, if applicable
git push origin
git checkout stable
git merge develop
mvn versions:set -DprocessAllModules
mvn clean deploy
git add pom.xml
git commit -m "vX.Y.Z poms"
git tag vX.Y.Z
git push origin
git push origin vX.Y.Z
git checkout develop
```
