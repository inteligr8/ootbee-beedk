/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ${package};

import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.security.authentication.AuthenticationUtil.RunAsWork;
import org.alfresco.repo.transaction.RetryingTransactionHelper.RetryingTransactionCallback;
import org.alfresco.service.ServiceRegistry;
import org.quartz.CronScheduleBuilder;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * This class is an example of how you can use the Alfresco Java Public API on a routine schedule.
 */
@Component
public class ExampleJob implements Job {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    
    // this is the Alfresco Public Java API entrypoint
	@Autowired
	private ServiceRegistry serviceRegistry;
	
	@Bean
	public JobDetail jobDetail() {
		return JobBuilder.newJob().ofType(this.getClass())
				.withIdentity("exampleJobName", "${groupId}.${artifactId}")
				.build();
	}
	
	@Bean
	public Trigger trigger(JobDetail jobDetail) { 
		return TriggerBuilder.newTrigger().forJob(jobDetail)
				.withIdentity("exampleTriggerName", "${groupId}.${artifactId}")
				.withSchedule(CronScheduleBuilder.dailyAtHourAndMinute(3, 30))
				.build();
	}
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		if (this.logger.isTraceEnabled())
			this.logger.trace("execute('" + context.getFireInstanceId() + "')");

		// provide some authority within the Alfresco context
		AuthenticationUtil.runAsSystem(new RunAsWork<Void>() {
			@Override
			public Void doWork() throws Exception {
				// provide expected hiccup/concurrency protection
				return ExampleJob.this.serviceRegistry.getRetryingTransactionHelper().doInTransaction(new RetryingTransactionCallback<Void>() {
					@Override
					public Void execute() throws Throwable {
						ExampleJob.this.fire(context);
						return null;
					}
				});
			}
		});
	}
	
	public void fire(JobExecutionContext context) throws JobExecutionException {
		if (this.logger.isTraceEnabled())
			this.logger.trace("fire('" + context.getFireInstanceId() + "')");
		
		// TODO do some work
	}

}
