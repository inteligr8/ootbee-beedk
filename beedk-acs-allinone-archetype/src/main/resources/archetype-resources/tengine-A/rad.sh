#!/bin/sh

discoverArtifactId() {
	ARTIFACT_ID=`mvn -q -Dexec.executable=echo -Dexec.args='${project.artifactId}' --non-recursive exec:exec 2>/dev/null`
}

rebuild() {
	echo "Rebuilding project ..."
	mvn process-test-classes
}

start() {
	echo "Rebuilding project and starting Docker containers to support rapid application development ..."
	mvn -Drad process-test-classes
}

stop() {
	discoverArtifactId
	echo "Stopping Docker containers that supported rapid application development ..."
	docker container ls --filter name=${ARTIFACT_ID}-*
	echo "Stopping containers ..."
	docker container stop `docker container ls -q --filter name=${ARTIFACT_ID}-*`
	echo "Removing containers ..."
	docker container rm `docker container ls -aq --filter name=${ARTIFACT_ID}-*`
}

tail_logs() {
	discoverArtifactId
	docker container logs -f `docker container ls -q --filter name=${ARTIFACT_ID}-$1`
}

list() {
	discoverArtifactId
	docker container ls --filter name=${ARTIFACT_ID}-*
}

case "$1" in
	start)
		start
		;;
	stop)
		stop
		;;
	restart)
		stop
		start
		;;
	rebuild)
		rebuild
		;;
	tail)
		tail_logs $2
		;;
	containers)
		list
		;;
	*)
		echo "Usage: ./rad.sh [ start | stop | restart | rebuild | tail {container} | containers ]"
		exit 1
esac

echo "Completed!"

