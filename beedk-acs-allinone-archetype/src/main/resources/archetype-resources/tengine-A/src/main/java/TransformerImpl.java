/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ${groupId};

import java.io.File;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.alfresco.transformer.executors.Transformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class TransformerImpl implements Transformer {
	
	private final Logger logger = LoggerFactory.getLogger(TransformerImpl.class);
	private final String id = "${shortname}";
	
	@PostConstruct
	public void init() throws Exception {
		if (this.logger.isDebugEnabled())
			this.logger.debug("init()");
	}
	
	@Override
	public String getTransformerId() {
		return this.id;
	}
	
	@Override
	public void extractMetadata(String transformName, String sourceMimetype, String targetMimetype, Map<String, String> transformOptions, File sourceFile, File targetFile) {
		if (this.logger.isTraceEnabled())
			this.logger.trace("extractMetadata('" + transformName + "', '" + sourceMimetype + "', '" + targetMimetype + "', " + transformOptions.keySet() + ", '" + sourceFile + "', '" + targetFile + "')");
		this.transform(transformName, sourceMimetype, targetMimetype, transformOptions, sourceFile, targetFile);
	}
	
	@Override
	public void transform(String transformName, String sourceMimetype, String targetMimetype, Map<String, String> transformOptions, File sourceFile, File targetFile) {
		if (this.logger.isTraceEnabled())
			this.logger.trace("transform('" + transformName + "', '" + sourceMimetype + "', '" + targetMimetype + "', " + transformOptions.keySet() + ", '" + sourceFile + "', '" + targetFile + "')");

		// TODO implement your transformation logic here
	}

}
