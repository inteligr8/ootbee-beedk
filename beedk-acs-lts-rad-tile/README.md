# BeeDK Alfresco Local Transform Services Maven Tile for RAD

This is a component within the proposed [BeeDK](/inteligr8/ootbee-beedk).  It is considered to be part of the **Public API** of the BeeDK.

## Purpose

This project creates a [Maven Tile](https://github.com/repaint-io/maven-tiles) that provides Alfresco Local Transform Services for the rapid application development of any ACS module, extension, service, or web application.

## Usage

To use this plugin, just reference it with the Maven Tiles plugin as shown in the following snippet.  Do not forget the `extensions` element in the *plugin* configuration.

```xml
<project ...>
	...
	<build>
		...
		<plugins>
			<plugin>
				<groupId>io.repaint.maven</groupId>
				<artifactId>tiles-maven-plugin</artifactId>
				<version>[2.0,3.0)</version>
				<extensions>true</extensions>
				<configuration>
					<tiles>
						...
						<tile>com.inteligr8.ootbee:beedk-acs-platform-<!-- See Below -->-rad-tile:[1.0.0,2.0.0)</tile>
						<!-- Documentation: https://bitbucket.org/inteligr8/ootbee-beedk/src/stable/beedk-acs-lts-rad-tile -->
						<tile>com.inteligr8.ootbee:beedk-acs-lts-rad-tile:[1.0.0,2.0.0)</tile>
						...
					</tiles>
				</configuration>
			</plugin>
			...
		</plugins>
		...
	</build>
	...
</project>
```

This tile is meant to be used in conjunction with the `beedk-acs-platform-rad-tile` tile which is automatically included in each of the following BeeDK Public API Maven Tiles.

*   [`beedk-acs-platform-self-rad-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-self-rad-tile)
*   [`beedk-acs-platform-artifact-rad-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-artifact-rad-tile)

> This Maven Tile should always be declared **AFTER** the Maven Tile used from the list above.

See the [BeeDK documentation](/inteligr8/ootbee-beedk) on instructions for how to start and stop RAD capabilities.

## Configuration

In your Maven project, set the following properties to define the behavior of this Maven Tile.  Unless otherwise stated, they can only be overridden in the project POM or other Maven Tiles loaded earlier than this Maven Tile.

### Public API

The following properties are intended to be exposed by inheriting Public API Maven Tiles.

| Maven Property         | Required | Default         | Description |
| ---------------------- |:--------:| --------------- | ----------- |
| `ate-aio.port`         |          | 8091            | The port to expose on `localhost` for the developer; not for other applications or users. |

### Other APIs

Additional less important configurations are inherited from the following Maven Tiles.

*   [`beedk-run-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-run-tile)
*   [`beedk-run-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-ate-test-tile)

## Results

The AIO Alfresco Transform Engine will be started during the `process-classes` Maven phase.

This tile will result in the definition of the following Maven properties during the respective Maven phase.

| Property                          | Data Type | Phase        | Description |
| --------------------------------- |:---------:|:------------:| ----------- |
| `beedk.rad.alts.enabled`          | Boolean   | *At startup* | `true`, which signals other Maven Tiles |
