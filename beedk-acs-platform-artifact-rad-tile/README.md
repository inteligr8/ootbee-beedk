# BeeDK ACS Platform RAD Maven Tile for non-Platform-accessible Projects

This is a component within the proposed [BeeDK](/inteligr8/ootbee-beedk).  It is considered to be part of the **Public API** of the BeeDK.

## Purpose

This project creates a [Maven Tile](https://github.com/repaint-io/maven-tiles) that provides the basis for rapid application development any Maven project against the ACS Platform.

## Usage

To use this plugin, just reference it with the Maven Tiles plugin as shown in the following snippet.  Do not forget the `extensions` element in the *plugin* configuration.

```xml
<project ...>
	...
	<build>
		...
		<plugins>
			<plugin>
				<groupId>io.repaint.maven</groupId>
				<artifactId>tiles-maven-plugin</artifactId>
				<version>[2.0,3.0)</version>
				<extensions>true</extensions>
				<configuration>
					<tiles>
						...
						<!-- Documentation: https://bitbucket.org/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-artifact-rad-tile -->
						<tile>com.inteligr8.ootbee:beedk-acs-platform-artifact-rad-tile:[1.0.0,2.0.0)</tile>
					</tiles>
				</configuration>
			</plugin>
			...
		</plugins>
		...
	</build>
	...
</project>
```

There are 2 other alternative tiles that serve a similar purpose, but are for other use cases.  Make sure you are using the right one.  Follow the logic below.

Is your Maven project an ACS Platform module or ACS Platform web application?
: You will be declaring the `beedk-acs-platform-module-tile` or `beedk-acs-platform-webapp-tile` Maven Tile.  You will need to explicitly declare and configure the [`beedk-acs-platform-self-rad-tile` Maven Tile](../beedk-acs-platform-self-rad-tile).

Is your Maven project just one within a set of other Maven projects AND one of those other Maven projects is an ACS Platform web application (not a module)?
: You will need to explicitly declare and configure the [`beedk-acs-platform-sibling-rad-tile` Maven Tile](../beedk-acs-platform-sibling-rad-tile).

Otherwise
: You will need to explicitly declare and configure the `beedk-acs-platform-artifact-rad-tile` Maven Tile.

See the [BeeDK documentation](/inteligr8/ootbee-beedk) on instructions for how to start and stop RAD capabilities.

## Configuration

In your Maven project, set the following properties to define the behavior of this Maven Tile.  Unless otherwise stated, they can only be overridden in the project POM or other Maven Tiles loaded earlier than this Maven Tile.

### Public API

The following properties are intended to be exposed by inheriting Public API Maven Tiles.

| Maven Property                     | Required | Default            | Description |
| ---------------------------------- |:--------:| ------------------ | ----------- |
| `alfresco.platform.version`        | Maybe    |                    | The Alfresco Platform version to use for RAD.  See below for more details. |
| `edition`                          |          | `community`        | The Alfresco Platform edition used for RAD.  `community` or `enterprise`.  See below for more details. |
| `alfresco.platform.war.groupId`    |          | `org.alfresco`     | The Maven `groupId` of a Maven `war` Type Project defined in a Maven Repository. |
| `alfresco.platform.war.artifactId` |          | *See Below*        | The Maven `artifactId` of a Maven `war` Type Project defined in a Maven Repository. |
| `alfresco.platform.war.version`    | Maybe    | *See Below*        | The Maven `version` of a Maven `war` Type Project defined in a Maven Repository. |
| `project.build.extDirectory`       |  | `${project.build.directory}/ext/platform` | An additional directory of JARs to add to the ACS Platform at runtime. |
| `acs-platform.port`                |          | 8080               | The port to expose on `localhost` for the developer; not for other applications or users.<br/>*Inherited from [`beedk-acs-platform-rad-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-rad-tile)* |
| `acs-platform.debugger.port`       |          | 8000               | The port to expose on `localhost` for the developer; not for other applications or users.<br/>*Inherited from [`beedk-acs-platform-rad-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-rad-tile)* |
| `acs-postgres.port`                |          | 5432               | The port to expose on `localhost` for the developer; not for other applications or users.<br/>*Inherited from [`beedk-acs-platform-rad-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-rad-tile)* |
| `acs-activemq.port`                |          | 8161               | The port to expose on `localhost` for the developer; not for other applications or users.<br/> *Inherited from [`beedk-acs-platform-rad-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-rad-tile)* |
| `acs-platform.tomcat.opts`         |          |                    | Additional `CATALINA_OPTS` to add to the Apache Tomcat startup.<br/>*May be overridden by any POM parent or Maven Tile.*<br/>*Inherited from [`beedk-acs-platform-rad-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-rad-tile)* |

*   The `edition` is used to default the `alfresco.platform.war.artifactId` to `content-services-community` or `content-services`.
*   The `alfresco.platform.version` is used to default the `alfresco.platform.war.version`.  It is expected to be used by other Maven Tiles, so it is here for convenience.  You must specify either property for this tile to function.


### Other APIs

Additional less important configurations are inherited from the following Maven Tiles.

*   [`beedk-acs-platform-rad-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-rad-tile)

## Results

The ACS Platform, database, and MQ components will be started during the `process-classes` Maven phase.
