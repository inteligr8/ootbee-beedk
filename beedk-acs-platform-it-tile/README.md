# BeeDK Abstract ACS Platform Maven Tile for Integration Tests

This is a component within the proposed [BeeDK](/inteligr8/ootbee-beedk).  It is considered to be part of the **Private API** of the BeeDK.

## Purpose

This project creates a [Maven Tile](https://github.com/repaint-io/maven-tiles) that provides the basis for integration testing any ACS module, extension, service, or web application.  It is intended to be used by other BeeDK components and not directly by any development project.

## Usage

To use this plugin, just reference it with the Maven Tiles plugin as shown in the following snippet.  Do not forget the `extensions` element in the *plugin* configuration.

```xml
<project ...>
	...
	<build>
		...
		<plugins>
			<plugin>
				<groupId>io.repaint.maven</groupId>
				<artifactId>tiles-maven-plugin</artifactId>
				<version>[2.0,3.0)</version>
				<extensions>true</extensions>
				<configuration>
					<tiles>
						...
						<!-- Documentation: https://bitbucket.org/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-it-tile -->
						<tile>com.inteligr8.ootbee:beedk-acs-platform-it-tile:[1.0.0,2.0.0)</tile>
					</tiles>
				</configuration>
			</plugin>
			...
		</plugins>
		...
	</build>
	...
</project>
```

## Configuration

In your Maven project, set the following properties to define the behavior of this Maven Tile.  Unless otherwise stated, they can only be overridden in the project POM or other Maven Tiles loaded earlier than this Maven Tile.

### Public API

The following properties are intended to be exposed by inheriting Public API Maven Tiles.

| Maven Property             | Required | Default         | Description |
| -------------------------- |:--------:| --------------- | ----------- |
| `acs-platform.port`        |          | 8080            | The port to expose on `localhost` for scripts to use during integration testing to verify the state of the application; not for a developer or user. |
| `acs-platform-ha.port`     |          | 8081            | The port to expose on `localhost` for scripts to use during integration testing to verify the state of the application; not for a developer or user. |
| `acs-postgres.port`        |          | 5432            | The port to expose on `localhost` for scripts to use during integration testing to verify the state of the application; not for a developer or user. |
| `acs-activemq.port`        |          | 8161            | The port to expose on `localhost` for scripts to use during integration testing to verify the state of the application; not for a developer or user. |
| `acs-platform.tomcat.opts` |          |                 | Additional `CATALINA_OPTS` to add to the Apache Tomcat startup.<br/>*May be overridden by any POM parent or Maven Tile.* |
| `acs-platform.timeout`     |          | 120000          | The time to wait for the startup to complete, in milliseconds. |
| `alfresco.license.directory` |  | `${user.home}/alfresco/license` | The base path to search for Alfresco licenses. |
| `acs.license.directory`    |  | `${alfresco.license.directory}/acs` | The base path to search for an ACS license. |
| `acs-postgres.version`     |          | *not important* | The version of PostgreSQL to use in the integration testing infrastructure. |
| `acs-activemq.version`     |          | *not important* | The version of Alfresco ActiveMQ to use in the integration testing infrastructure. |

### Private API

The following properties are only intended to be defined by BeeDK components.

| Maven Property                       | Required | Default            | Description |
| ------------------------------------ |:--------:| ------------------ | ----------- |
| `beedk.deploy.platform.warFile`      | **Yes**  |                    | The WAR file to deploy in the Apache Tomcat instance. |
| `beedk.deploy.platform.testClassesDirectory` |  |                    | An additional test classpath directory to sideload into the web application. |
| `beedk.deploy.platform.extDirectory` |          |                    | An additional directory of JARs to sideload into the web application. |
| `beedk.deploy.platform.dataDirectory` |  | `${project.build.directory}/alf_data`  | The temporary binary storage location for ACS. |
| `beedk.deploy.acs-ha.enabled`        |          | false              | Start an additional instance of the ACS platform for cluster-related testing. |
| `beedk.deploy.acs-search.enabled`    |          | false              | **Do not** set explicitly; just include the [`beedk-acs-search-it-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-search-it-tile) tile **AFTER** this tile. |
| `beedk.deploy.alts.enabled`          |          | false              | **Do not** set explicitly; just include the [`beedk-acs-lts-it-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-lts-it-tile) tile **AFTER** this tile. |
| `beedk.deploy.ats.enabled`           |          | false              | **Do not** set explicitly; just include the [`beedk-ats-it-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-ats-it-tile) tile **AFTER** this tile. |
| `tomcat-rad.version`                 |          | *not important*    | The version of the [Apache Tomcat Rapid Application Development Docker container](/inteligr8/tomcat-rad-docker). |

### Other APIs

Additional less important configurations are inherited from the following Maven Tiles.

*   [`beedk-run-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-run-tile)

## Results

The ACS Platform, database, and MQ components will be started and stopped during the `pre-integration-test` and `post-integration-test` Maven phases, respectively.
