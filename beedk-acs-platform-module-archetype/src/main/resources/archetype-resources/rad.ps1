
function discoverArtifactId {
	$script:ARTIFACT_ID=(mvn -q -Dexpression=project"."artifactId -DforceStdout help:evaluate)
}

function rebuild {
	echo "Rebuilding project ..."
	mvn process-test-classes
}

function start_ {
	echo "Rebuilding project and starting Docker containers to support rapid application development ..."
	mvn -Drad process-test-classes
}

function start_log {
	echo "Rebuilding project and starting Docker containers to support rapid application development ..."
	mvn -Drad "-Ddocker.showLogs" process-test-classes
}

function stop_ {
	discoverArtifactId
	echo "Stopping Docker containers that supported rapid application development ..."
	docker container ls --filter name="^/${ARTIFACT_ID}"
	echo "Stopping containers ..."
	docker container stop (docker container ls -q --filter name="^/${ARTIFACT_ID}")
	echo "Removing containers ..."
	docker container rm (docker container ls -aq --filter name="^/${ARTIFACT_ID}")
}

function tail_logs {
	param (
		$container
	)
	
	discoverArtifactId
	docker container logs -f (docker container ls -q --filter name="^/${ARTIFACT_ID}-${container}$")
}

function list {
	discoverArtifactId
	docker container ls --filter name="^/${ARTIFACT_ID}"
}

switch ($args[0]) {
	"start" {
		start_
	}
	"start_log" {
		start_log
	}
	"stop" {
		stop_
	}
	"restart" {
		stop_
		start_
	}
	"rebuild" {
		rebuild
	}
	"tail" {
		tail_logs $args[1]
	}
	"containers" {
		list
	}
	default {
		echo "Usage: .\rad.ps1 [ start | start_log | stop | restart | rebuild | tail {container} | containers ]"
	}
}

echo "Completed!"

