/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ${package};

import java.util.List;

import org.alfresco.repo.action.ParameterDefinitionImpl;
import org.alfresco.repo.action.executer.ActionExecuterAbstractBase;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ParameterDefinition;
import org.alfresco.service.cmr.dictionary.DataTypeDefinition;
import org.alfresco.service.cmr.repository.NodeRef;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * This class is an example of how you can use the Alfresco Java Public API in Alfresco Actions.
 * 
 * The bean must be declared in the XML and not through annotations.
 */
public class ExampleActionExecuter extends ActionExecuterAbstractBase {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
    // this is the Alfresco Public Java API entrypoint
    @Autowired
    protected ServiceRegistry serviceRegistry;
    
    @Override
	protected void addParameterDefinitions(List<ParameterDefinition> paramList) {
    	paramList.add(new ParameterDefinitionImpl("example.param", DataTypeDefinition.TEXT, false, "Param #1"));
	}
    
    @Override
    protected void executeImpl(Action action, NodeRef actionedUponNodeRef) {
    	this.logger.trace("executeImpl({})", actionedUponNodeRef);
    	
    	String value = (String)action.getParameterValue("example.param");
    	// TODO do some work
    }

}
