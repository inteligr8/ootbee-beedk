/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ${package};

import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.security.authentication.AuthenticationUtil.RunAsWork;
import org.alfresco.repo.transaction.RetryingTransactionHelper.RetryingTransactionCallback;
import org.alfresco.service.ServiceRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEvent;
import org.springframework.extensions.surf.util.AbstractLifecycleBean;
import org.springframework.stereotype.Component;

/**
 * This class is an example of how you can use the Alfresco Java Public API on startup or shutdown.
 */
@Component
public class ExampleBootstrap extends AbstractLifecycleBean {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
    // this is the Alfresco Public Java API entrypoint
    @Autowired
    protected ServiceRegistry serviceRegistry;
    
    @Value("${example.config.value:default}")
    private String configValue;
    
    @Value("${example.config.value:#{null}}")
    private String configValueWithNullDefault;
    
    /**
     * This is called after Spring beans and configuration are injected, but
     * before ACS services are ready.  So don't call any ACS services, start an
     * ACS transaction, or try to create an authentication context.
     */
    @PostConstruct
    private void init() {
    }
    
    /**
     * This is called after @PostConstruct and all the ACS services are
     * initialized and ready.  You will have no authentication or transaction
     * context.
     */
    @Override
    protected void onBootstrap(ApplicationEvent event) {
    	this.logger.trace("onBootstrap()");
		
		// provide some authority within the Alfresco context
		AuthenticationUtil.runAsSystem(new RunAsWork<Void>() {
			@Override
			public Void doWork() throws Exception {
				// provide expected hiccup/concurrency protection
				return ExampleBootstrap.this.serviceRegistry.getRetryingTransactionHelper().doInTransaction(new RetryingTransactionCallback<Void>() {
					@Override
					public Void execute() throws Throwable {
						ExampleBootstrap.this.onStartup();
						return null;
					}
				});
			}
		});
	}

    /**
     * This is called after @PostConstruct and all the ACS services are
     * initialized and ready.  You will have no authentication or transaction
     * context.
     */
    @Override
    protected void onShutdown(ApplicationEvent event) {
    	this.logger.trace("onShutdown()");
		
		// provide some authority within the Alfresco context
		AuthenticationUtil.runAsSystem(new RunAsWork<Void>() {
			@Override
			public Void doWork() throws Exception {
				// provide expected hiccup/concurrency protection
				return ExampleBootstrap.this.serviceRegistry.getRetryingTransactionHelper().doInTransaction(new RetryingTransactionCallback<Void>() {
					@Override
					public Void execute() throws Throwable {
						ExampleBootstrap.this.onShutdown();
						return null;
					}
				});
			}
		});
	}
	
	public void onStartup() {
		// TODO do some work
	}
	
	public void onShutdown() {
		// TODO do some work
	}

}
