/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ${package};

import java.io.Serializable;
import java.util.Map;

import org.alfresco.repo.node.NodeServicePolicies.OnCreateNodePolicy;
import org.alfresco.repo.node.NodeServicePolicies.OnUpdatePropertiesPolicy;
import org.alfresco.repo.policy.annotation.Behaviour;
import org.alfresco.repo.policy.annotation.BehaviourBean;
import org.alfresco.repo.policy.annotation.BehaviourKind;
import org.alfresco.repo.policy.Behaviour.NotificationFrequency;
import org.alfresco.repo.policy.JavaBehaviour;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.namespace.QName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.surf.util.AbstractLifecycleBean;
import org.springframework.stereotype.Component;

/**
 * This class is an example of how you can use the Alfresco Java Public API when Alfresco triggers an event.
 */
@Component
@BehaviourBean
public class ExampleEventListener extends AbstractLifecycleBean implements OnCreateNodePolicy, OnUpdatePropertiesPolicy {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
    // this is the Alfresco Public Java API entrypoint
    @Autowired
    protected ServiceRegistry serviceRegistry;
	
    @Override
    protected void onBootstrap(ApplicationEvent event) {
		this.bind();
	}

	/**
	 * This must be called after the `PolicyComponent` is initialized/ready.
	 * So you cannot call it from with `@PostConstruct` or using
	 * `InitializingBean`.
	 */
	public void bind() {
		this.logger.trace("bind()");

		// example listener
		this.serviceRegistry.getPolicyComponent().bindClassBehaviour(OnCreateNodePolicy.QNAME, this,
				new JavaBehaviour(this, OnCreateNodePolicy.QNAME.getLocalName(), NotificationFrequency.TRANSACTION_COMMIT));
	}
	
	/**
	 * This will execute with the same authentication and transaction context
	 * as was used to create the node, unless the binding is
	 * `TRANSACTION_COMMIT`.  Then it will only have the same transaction
	 * context and the authentication context used when starting the
	 * transaction.
	 */
	@Override
	public void onCreateNode(ChildAssociationRef childAssocRef) {
		this.logger.trace("onCreateNode({})", childAssocRef.getChildRef());
		
		// TODO do some work
	}

	@Behaviour(kind = BehaviorKind.CLASS, NotificationFrequency = NotificationFrequency.FIRST_EVENT)
	public void onUpdateProperties(NodeRef nodeRef, Map<QName, Serializable> before, Map<QName, Serializable> after) {
		this.logger.trace("onUpdateProperties({})", nodeRef);
		
		// TODO do some work

	}

}
