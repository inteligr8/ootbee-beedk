/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ${package};

import org.alfresco.service.ServiceRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * This class is an example of how you can use the Alfresco Java Public API by listening for external ReST requests.
 * 
 * The component value (bean ID) must start with 'webscript.' and end with '.{rest_method}'.
 */
@Component(value = "webscript.${package}.exampleJava.get")
public class ExampleGetWebScript extends AbstractWebScript {
    
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    // this is the Alfresco Public Java API entrypoint
    @Autowired
    protected ServiceRegistry serviceRegistry;

    /**
     * This will have the authentication context of the authenticated user
     * calling the REST endpoint.  The transaction context is defined in the
     * web script descriptor.
     */
    @Override
    public void execute(WebScriptRequest req, WebScriptResponse res) throws IOException {
		this.logger.trace("execute()");
		
        // TODO do some work
    }
}
