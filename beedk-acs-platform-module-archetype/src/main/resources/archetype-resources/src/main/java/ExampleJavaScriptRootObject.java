/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ${package};

import org.alfresco.repo.processor.BaseProcessorExtension;
import org.alfresco.service.ServiceRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * This class is an example of how you can use the Alfresco Java Public API to extend the JavaScript engine.
 * 
 * The bean must be declared in the XML and not through annotations.
 */
public class ExampleJavaScriptRootObject extends BaseProcessorExtension {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
    // this is the Alfresco Public Java API entrypoint
    @Autowired
    protected ServiceRegistry serviceRegistry;
    
    /**
     * An example method that called from within a JavaScript within the ACS
     * application: `example.getMessage("example.property")`.  All simple
     * parameter and return types are generally supported.
     * 
     * The `example` root name comes from the `module-context.xml` file.
     */
    public String getMessage(String messageKey) {
    	this.logger.trace("getMessage({})", messageKey);
    	
    	return this.serviceRegistry.getDictionaryService().getMessage(messageKey);
    }

}
