# BeeDK ACS Platform Module Maven Tile

This is a component within the proposed [BeeDK](/inteligr8/ootbee-beedk).  It is considered to be part of the **Public API** of the BeeDK.

## Purpose

This project creates a [Maven Tile](https://github.com/repaint-io/maven-tiles) that provides the basis for any ACS Platform module or extension.

## Usage

To use this plugin, just reference it with the Maven Tiles plugin as shown in the following snippet.  Do not forget the `extensions` element in the *plugin* configuration.

```xml
<project ...>
	...
	<build>
		...
		<plugins>
			<plugin>
				<groupId>io.repaint.maven</groupId>
				<artifactId>tiles-maven-plugin</artifactId>
				<version>[2.0,3.0)</version>
				<extensions>true</extensions>
				<configuration>
					<tiles>
						...
						<!-- Documentation: https://bitbucket.org/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-module-tile -->
						<tile>com.inteligr8.ootbee:beedk-acs-platform-module-tile:[1.0.0,2.0.0)</tile>
						...
					</tiles>
				</configuration>
			</plugin>
			...
		</plugins>
		...
	</build>
	...
</project>
```

## Configuration

In your Maven project, set the following properties to define the behavior of this Maven Tile.  Unless otherwise stated, they can only be overridden in the project POM or other Maven Tiles loaded earlier than this Maven Tile.

### Public API

The following properties are intended to be exposed by inheriting Public API Maven Tiles.

| Maven Property                     | Required | Default            | Description |
| ---------------------------------- |:--------:| ------------------ | ----------- |
| `project.build.packageDirectory`   |          | `target/package`   | A variable specifying the folder name of consolidated resources for packaging. |
| `alfresco.module.pathname`         |          | `${project.groupId}.${project.artifactId}` | A variable specifying the folder name of module resources.  Use this property to help reference module resources.  It should rarely ever be overridden. |

### Other APIs

Additional less important configurations are inherited from the following Maven Tile.

*   [`beedk-acs-module-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-module-tile)

## Results

The project will build as a deployable ACS Platform module.

This Maven Tile will result in the definition of the following Maven properties during the respective Maven phase.

| Property                     | Data Type | Phase        | Description |
| ---------------------------- |:---------:|:------------:| ----------- |
| `alfresco.module.path`       | Text      | *At startup* | The classpath to the module resources: `alfresco/module/${alfresco.module.pathname}` |
| `alfresco.ext.path`          | Text      | *At startup* | The classpath to the extension resources: `alfresco/extension` |
| `alfresco.ext.package`       | Text      | *At startup* | The package reference to the extension resources: `alfresco.extension` |

## Initialization

This tile comes with the ability to initialize your project with the required file(s) of an ACS Platform module.  You can do this by executing any Maven build with the `scaffold` property.

```sh
mvn -Dscaffold generate-sources
```
