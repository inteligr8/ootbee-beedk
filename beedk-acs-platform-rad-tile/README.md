# BeeDK Abstract ACS Platform Maven Tile for RAD

This is a component within the proposed [BeeDK](/inteligr8/ootbee-beedk).  It is considered to be part of the **Private API** of the BeeDK.

## Purpose

This project creates a [Maven Tile](https://github.com/repaint-io/maven-tiles) that provides the basis for rapid application development of any ACS module, extension, service, or web application.  It is intended to be used by other BeeDK components and not directly by any development project.

## Usage

To use this plugin, just reference it with the Maven Tiles plugin as shown in the following snippet.  Do not forget the `extensions` element in the *plugin* configuration.

```xml
<project ...>
	...
	<build>
		...
		<plugins>
			<plugin>
				<groupId>io.repaint.maven</groupId>
				<artifactId>tiles-maven-plugin</artifactId>
				<version>[2.0,3.0)</version>
				<extensions>true</extensions>
				<configuration>
					<tiles>
						...
						<!-- Documentation: https://bitbucket.org/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-rad-tile -->
						<tile>com.inteligr8.ootbee:beedk-acs-platform-rad-tile:[1.0.0,2.0.0)</tile>
					</tiles>
				</configuration>
			</plugin>
			...
		</plugins>
		...
	</build>
	...
</project>
```

See the [BeeDK documentation](/inteligr8/ootbee-beedk) on instructions for how to start and stop RAD capabilities.

## Configuration

In your Maven project, set the following properties to define the behavior of this Maven Tile.  Unless otherwise stated, they can only be overridden in the project POM or other Maven Tiles loaded earlier than this Maven Tile.

### Public API

The following properties are intended to be exposed by inheriting Public API Maven Tiles.

| Maven Property             | Required | Default         | Description |
| -------------------------- |:--------:| --------------- | ----------- |
| `acs-platform.hotswap.enabled` |      | true            | Enable the HotSwap Agent for live classpath reloading. |
| `acs-platform.hotswap.disablePlugins` | | Hibernate     | Do not enable the HotSwap Agent with the following plugins.  All plugins are documented here: https://github.com/HotswapProjects/HotswapAgent/tree/master/plugin |
| `acs-platform.debugger.enabled` |     | true            | Enable the JDWP debugger. |
| `acs-platform.port`        |          | 8080            | The port to expose on `localhost` for the developer; not for other applications or users. |
| `acs-postgres.port`        |          | 5432            | The port to expose on `localhost` for the developer; not for other applications or users. |
| `acs-activemq.port`        |          | 8161            | The port to expose on `localhost` for the developer; not for other applications or users. |
| `acs-platform.debugger.port` |        | 8000            | The port to expose on `localhost` for the developer; not for other applications or users. |
| `acs-platform.tomcat.opts` |          |                 | Additional `CATALINA_OPTS` to add to the Apache Tomcat startup.<br/>*May be overridden by any POM parent or Maven Tile.* |
| `acs-platform.timeout`     |          | 120000          | The time to wait for the startup to complete, in milliseconds. |
| `alfresco.license.directory` |  | `${user.home}/alfresco/license` | The base path to search for Alfresco licenses. |
| `acs.license.directory`    |  | `${alfresco.license.directory}/acs` | The base path to search for an ACS license. |
| `tomcat-rad.version`       |          | *not important* | The version of the [Apache Tomcat Rapid Application Development Docker container](/inteligr8/tomcat-rad-docker). |
| `acs-postgres.version`     |          | *not important* | The version of PostgreSQL to use in the integration testing infrastructure. |
| `acs-activemq.version`     |          | *not important* | The version of Alfresco ActiveMQ to use in the integration testing infrastructure. |
| `acs-api-explorer.war.groupId` |      | `org.alfresco`  | The API Explorer WAR Maven Group ID. |
| `acs-api-explorer.war.artifactId` |   | `api-explorer`  | The API Explorer WAR Maven Artifact ID. |
| `acs-api-explorer.war.version` | | `${acs-api-explorer.version}` | The API Explorer WAR Maven version.  See the [Alfresco Maven Repository](https://artifacts.alfresco.com/nexus/content/groups/public/org/alfresco/api-explorer/) for details on available versions. |
| `acs-aos.war.version`      |          | `1.3.2.1`       | The Alfresco Office Services WAR Maven version.  See the [Alfresco Maven Repository](https://artifacts.alfresco.com/nexus/content/groups/public/org/alfresco/aos-module/alfresco-vti-bin/) for details on available versions. |

### Private API

The following properties are only intended to be defined by BeeDK components.

| Maven Property                       | Required | Default            | Description |
| ------------------------------------ |:--------:| ------------------ | ----------- |
| `beedk.deploy.platform.warFile`      | **Yes**  |                    | The WAR file to deploy in the Apache Tomcat instance. |
| `beedk.deploy.platform.classesDirectory`     |  |                    | An additional classpath directory to sideload into the web application. |
| `beedk.deploy.platform.testClassesDirectory` |  |                    | An additional test classpath directory to sideload into the web application. |
| `beedk.deploy.platform.extDirectory` |          |                    | An additional directory of JARs to sideload into the web application. |
| `beedk.deploy.platform.webDirectory` |          |                    | An additional directory of web resources to sideload in the web application. |
| `beedk.deploy.platform.warDirectory` |  | `${project.build.warDirectory}` | A build directory for WAR files. |
| `beedk.deploy.platform.dataDirectory` |  | `${project.build.directory}/alf_data`  | The temporary binary storage location for ACS. |
| `beedk.deploy.api-explorer.warFile`  |          | *Not important*    | The API Explorer WAR file to deploy in the Apache Tomcat instance. |
| `beedk.rad.acs-search.enabled`       |          | false              | **Do not** set explicitly; just include the [`beedk-acs-search-rad-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-search-rad-tile) tile **AFTER** this tile. |
| `beedk.rad.alts.enabled`             |          | false              | **Do not** set explicitly; just include the [`beedk-acs-lts-rad-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-lts-rad-tile) tile **AFTER** this tile. |
| `beedk.rad.ats.enabled`              |          | false              | **Do not** set explicitly; just include the [`beedk-ats-rad-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-ats-rad-tile) tile **AFTER** this tile. |
| `acs-api-explorer.version`           |          | `7.4.2`           | The API Explorer version.  You could use `${alfresco.platform.version}` in ACS Enterprise.  This will eventually parse the ACS Platform version for the right value. |

### Other APIs

Additional less important configurations are inherited from the following Maven Tiles.

*   [`beedk-run-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-run-tile)

## Results

The ACS Platform, database, and MQ components will be started during the `process-classes` Maven phase.
