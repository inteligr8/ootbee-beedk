# BeeDK ACS Platform Integration Test Maven Tile for Platform Projects

This is a component within the proposed [BeeDK](/inteligr8/ootbee-beedk).  It is considered to be part of the **Private API** of the BeeDK.

## Purpose

This project creates a [Maven Tile](https://github.com/repaint-io/maven-tiles) that provides the basis for integration testing any ACS Platform module, extension, service, or web application.

## Usage

To use this plugin, just reference it with the Maven Tiles plugin as shown in the following snippet.  Do not forget the `extensions` element in the *plugin* configuration.

```xml
<project ...>
	...
	<build>
		...
		<plugins>
			<plugin>
				<groupId>io.repaint.maven</groupId>
				<artifactId>tiles-maven-plugin</artifactId>
				<version>[2.0,3.0)</version>
				<extensions>true</extensions>
				<configuration>
					<tiles>
						...
						<!-- Documentation: https://bitbucket.org/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-self-it-tile -->
						<tile>com.inteligr8.ootbee:beedk-acs-platform-self-it-tile:[1.0.0,2.0.0)</tile>
					</tiles>
				</configuration>
			</plugin>
			...
		</plugins>
		...
	</build>
	...
</project>
```

There are 2 other alternative tiles that serve a similar purpose, but are for other use cases.  Make sure you are using the right one.  Follow the logic below.

Is your Maven project an ACS Platform module or ACS Platform web application?
: You will be declaring the `beedk-acs-platform-module-tile` or `beedk-acs-platform-webapp-tile` Maven Tile.  You will need to explicitly declare and configure this `beedk-acs-platform-self-it-tile` Maven Tile.

Is your Maven project just one within a set of other Maven projects AND one of those other Maven projects is an ACS Platform web application (not a module)?
: You will need to explicitly declare and configure the [`beedk-acs-platform-sibling-it-tile` Maven Tile](../beedk-acs-platform-sibling-it-tile).

Otherwise
: You will need to explicitly declare and configure the [`beedk-acs-platform-artifact-it-tile` Maven Tile](../beedk-acs-platform-artifact-it-tile).

## Configuration

In your Maven project, set the following properties to define the behavior of this Maven Tile.  Unless otherwise stated, they can only be overridden in the project POM or other Maven Tiles loaded earlier than this Maven Tile.

### Public API

The following properties are intended to be exposed by inheriting Public API Maven Tiles.

| Maven Property                     | Required | Default            | Description |
| ---------------------------------- |:--------:| ------------------ | ----------- |
| `alfresco.sdk.version`             | **YES**  |                    | For a list of versions and more details, see the [Alfresco SDK](https://github.com/Alfresco/alfresco-sdk).<br/>*Inherited from [`beedk-acs-webapp-artifact-it-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-webapp-artifact-it-tile)* |
| `alfresco.platform.version`        | *Maybe*  |                    | The Alfresco Platform version to use for integration testing.  See below for more details. |
| `edition`                          |          | `community`        | The Alfresco Platform edition to use for integration testing.  `community` or `enterprise`.  See below for more details. |
| `alfresco.war.version`             | *Maybe*  | *See Below*        | The Maven `version` of a Maven `war` Type Project defined in a Maven Repository.<br/>*Inherited from [`beedk-acs-webapp-artifact-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-webapp-artifact-tile)* |
| `project.build.extDirectory`       |  | `${project.build.directory}/ext/platform` | An additional directory of JARs to add to the ACS Platform at runtime. |
| `acs-platform.port`                |          | 8080               | The port to expose on `localhost` for scripts to use during integration testing to verify the state of the application; not for a developer or user.<br/>*Inherited from [`beedk-acs-platform-it-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-it-tile)* |
| `acs-postgres.port`                |          | 5432               | The port to expose on `localhost` for scripts to use during integration testing to verify the state of the application; not for a developer or user.<br/>*Inherited from [`beedk-acs-platform-it-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-it-tile)* |
| `acs-activemq.port`                |          | 8161               | The port to expose on `localhost` for scripts to use during integration testing to verify the state of the application; not for a developer or user.<br/> *Inherited from [`beedk-acs-platform-it-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-it-tile)* |
| `acs-platform.tomcat.opts`         |          |                    | Additional `CATALINA_OPTS` to add to the Apache Tomcat startup.<br/>*May be overridden by any POM parent or Maven Tile.*<br/>*Inherited from [`beedk-acs-platform-it-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-it-tile)* |

*   The `edition` is used to default the `alfresco.war.artifactId` to `content-services-community` or `content-services`.
*   The `alfresco.platform.version` is used to default the `alfresco.platform.war.version`.  It is expected to be used by other Maven Tiles, so it is here for convenience.  You must specify either property for this tile to function.

### Other APIs

Additional less important configurations are inherited from the following Maven Tiles.

*   [`beedk-acs-platform-it-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-it-tile)
*   [`beedk-acs-webapp-artifact-it-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-webapp-artifact-it-tile)

## Results

The ACS Platform, database, and MQ components will be started and stopped during the `pre-integration-test` and `post-integration-test` Maven phases, respectively.
