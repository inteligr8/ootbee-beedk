# BeeDK ACS Platform RAD Maven Tile for non-Platform Projects

This is a component within the proposed [BeeDK](/inteligr8/ootbee-beedk).  It is considered to be part of the **Public API** of the BeeDK.

## Purpose

This project creates a [Maven Tile](https://github.com/repaint-io/maven-tiles) that provides the basis for the rapid application development of any Maven project against the ACS Platform.

## Usage

To use this plugin, just reference it with the Maven Tiles plugin as shown in the following snippet.  Do not forget the `extensions` element in the *plugin* configuration.

```xml
<project ...>
	...
	<build>
		...
		<plugins>
			<plugin>
				<groupId>io.repaint.maven</groupId>
				<artifactId>tiles-maven-plugin</artifactId>
				<version>[2.0,3.0)</version>
				<extensions>true</extensions>
				<configuration>
					<tiles>
						...
						<!-- Documentation: https://bitbucket.org/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-sibling-rad-tile -->
						<tile>com.inteligr8.ootbee:beedk-acs-platform-sibling-rad-tile:[1.0.0,2.0.0)</tile>
					</tiles>
				</configuration>
			</plugin>
			...
		</plugins>
		...
	</build>
	...
</project>
```

There are 2 other alternative tiles that serve a similar purpose, but are for other use cases.  Make sure you are using the right one.  Follow the logic below.

Is your Maven project an ACS Platform module or ACS Platform web application?
: You will be declaring the `beedk-acs-platform-module-tile` or `beedk-acs-platform-webapp-tile` Maven Tile.  You will need to explicitly declare and configure the [`beedk-acs-platform-self-rad-tile` Maven Tile](../beedk-acs-platform-self-rad-tile).

Is your Maven project just one within a set of other Maven projects AND one of those other Maven projects is an ACS Platform web application (not a module)?
: You will need to explicitly declare and configure the `beedk-acs-platform-sibling-rad-tile` Maven Tile.

Otherwise
: You will need to explicitly declare and configure the [`beedk-acs-platform-artifact-rad-tile` Maven Tile](../beedk-acs-platform-artifact-rad-tile).

See the [BeeDK documentation](/inteligr8/ootbee-beedk) on instructions for how to start and stop RAD capabilities.

## Configuration

In your Maven project, set the following properties to define the behavior of this Maven Tile.  Unless otherwise stated, they can only be overridden in the project POM or other Maven Tiles loaded earlier than this Maven Tile.

### Public API

The following properties are intended to be exposed by inheriting Public API Maven Tiles.

| Maven Property                     | Required | Default            | Description |
| ---------------------------------- |:--------:| ------------------ | ----------- |
| `project.sibling.name`             | **Yes**  |                    | The name of a sibling Platform module or web application project. |
| `project.sibling.build.directory`  |  | `../${project.sibling.name}/target` | The traditional `project.build.directory` of the sibling Platform module or web appliation project. |
| `project.sibling.build.extDirectory` |  | `${project.sibling.build.directory}/ext/platform` | An additional directory of JARs to sideload into the web application. |
| `project.sibling.warFileName`      | **Yes**  |                    | The filename of the sibling Platform module or web appliation WAR. |
| `project.sibling.build.warDirectory` |  | `${project.sibling.build.directory}/war` | The path of the sibling Platform module or web appliation WAR. |
| `project.sibling.build.warFile`    |  | `${project.sibling.build.warDirectory}/${project.sibling.warFileName}` | The path and filename of the sibling Platform module or web appliation WAR. |
| `acs-platform.port`                |          | 8080               | The port to expose on `localhost` for the developer; not for other applications or users.<br/>*Inherited from [`beedk-acs-platform-rad-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-rad-tile)* |
| `acs-platform.debugger.port`       |          | 8000               | The port to expose on `localhost` for the developer; not for other applications or users.<br/>*Inherited from [`beedk-acs-platform-rad-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-rad-tile)* |
| `acs-postgres.port`                |          | 5432               | The port to expose on `localhost` for the developer; not for other applications or users.<br/>*Inherited from [`beedk-acs-platform-rad-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-rad-tile)* |
| `acs-activemq.port`                |          | 8161               | The port to expose on `localhost` for the developer; not for other applications or users.<br/> *Inherited from [`beedk-acs-platform-rad-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-rad-tile)* |
| `acs-platform.tomcat.opts`         |          |                    | Additional `CATALINA_OPTS` to add to the Apache Tomcat startup.<br/>*May be overridden by any POM parent or Maven Tile.*<br/>*Inherited from [`beedk-acs-platform-rad-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-rad-tile)* |

### Other APIs

Additional less important configurations are inherited from the following Maven Tiles.

*   [`beedk-acs-platform-rad-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-rad-tile)

## Results

The ACS Platform, database, and MQ components will be started during the `process-classes` Maven phase.
