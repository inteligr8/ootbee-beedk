# BeeDK ACS Platform Web Application Maven Tile

This is a component within the proposed [BeeDK](/inteligr8/ootbee-beedk).  It is considered to be part of the **Public API** of the BeeDK.

## Purpose

This project creates a [Maven Tile](https://github.com/repaint-io/maven-tiles) that provides the basis for any ACS Platform web application.

## Usage

To use this plugin, just reference it with the Maven Tiles plugin as shown in the following snippet.  Do not forget the `extensions` element in the *plugin* configuration.

```xml
<project ...>
	...
	<build>
		...
		<plugins>
			<plugin>
				<groupId>io.repaint.maven</groupId>
				<artifactId>tiles-maven-plugin</artifactId>
				<version>[2.0,3.0)</version>
				<extensions>true</extensions>
				<configuration>
					<tiles>
						...
						<!-- Documentation: https://bitbucket.org/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-webapp-tile -->
						<tile>com.inteligr8.ootbee:beedk-acs-platform-webapp-tile:[1.0.0,2.0.0)</tile>
						...
					</tiles>
				</configuration>
			</plugin>
			...
		</plugins>
		...
	</build>
	...
</project>
```

## Configuration

In your Maven project, set the following properties to define the behavior of this Maven Tile.  Unless otherwise stated, they can only be overridden in the project POM or other Maven Tiles loaded earlier than this Maven Tile.

### Public API

The following properties are intended to be exposed by inheriting Public API Maven Tiles.

| Maven Property                     | Required | Default            | Description |
| ---------------------------------- |:--------:| ------------------ | ----------- |
| `alfresco.sdk.version`             | **YES**  |                    | For a list of versions and more details, see the [Alfresco SDK](https://github.com/Alfresco/alfresco-sdk).<br/>*Inherited from [`beedk-acs-webapp-artifact-build-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-webapp-artifact-build-tile)* |

### Other APIs

Additional less important configurations are inherited from the following Maven Tiles.

    *   [`beedk-acs-webapp-artifact-build-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-webapp-artifact-build-tile)
