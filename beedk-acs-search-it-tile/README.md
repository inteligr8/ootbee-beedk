# BeeDK Alfresco Search Services Maven Tile for Integration Tests

This is a component within the proposed [BeeDK](/inteligr8/ootbee-beedk).  It is considered to be part of the **Public API** of the BeeDK.

## Purpose

This project creates a [Maven Tile](https://github.com/repaint-io/maven-tiles) that provides Alfresco Search Services for the integration testing of any ACS module, extension, service, or web application.

## Usage

To use this plugin, just reference it with the Maven Tiles plugin as shown in the following snippet.  Do not forget the `extensions` element in the *plugin* configuration.

```xml
<project ...>
	...
	<build>
		...
		<plugins>
			<plugin>
				<groupId>io.repaint.maven</groupId>
				<artifactId>tiles-maven-plugin</artifactId>
				<version>[2.0,3.0)</version>
				<extensions>true</extensions>
				<configuration>
					<tiles>
						...
						<tile>com.inteligr8.ootbee:beedk-acs-<!-- See below -->-tile:[1.0.0,2.0.0)</tile>
						<!-- Documentation: https://bitbucket.org/inteligr8/ootbee-beedk/src/stable/beedk-acs-search-it-tile -->
						<tile>com.inteligr8.ootbee:beedk-acs-search-it-tile:[1.0.0,2.0.0)</tile>
						...
					</tiles>
				</configuration>
			</plugin>
			...
		</plugins>
		...
	</build>
	...
</project>
```

This tile is meant to be used in conjunction with the `beedk-acs-platform-it-tile` tile which is automatically included in each of the following BeeDK Public API Maven Tiles.

*   [`beedk-acs-platform-module-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-module-tile)
*   [`beedk-acs-share-module-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-share-module-tile)
*   [`beedk-acs-platform-webapp-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-webapp-tile)
*   [`beedk-acs-share-webapp-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-share-webapp-tile)

> This Maven Tile should always be declared **AFTER** the Maven Tile used from the list above.

## Configuration

In your Maven project, set the following properties to define the behavior of this Maven Tile.  Unless otherwise stated, they can only be overridden in the project POM or other Maven Tiles loaded earlier than this Maven Tile.

### Other APIs

Additional less important configurations are inherited from the following Maven Tiles.

*   [`beedk-run-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-run-tile)
*   [`beedk-run-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-search-test-tile)

## Results

The Alfresco Search Service will be started and stopped during the `pre-integration-test` and `post-integration-test` Maven phases, respectively.

This tile will result in the definition of the following Maven properties during the respective Maven phase.

| Property                          | Data Type | Phase        | Description |
| --------------------------------- |:---------:|:------------:| ----------- |
| `beedk.deploy.acs-search.enabled` | Boolean   | *At startup* | `true`, which signals other Maven Tiles |
