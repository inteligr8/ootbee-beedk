# BeeDK Abstract ACS Share Maven Tile for RAD

This is a component within the proposed [BeeDK](/inteligr8/ootbee-beedk).  It is considered to be part of the **Private API** of the BeeDK.

## Purpose

This project creates a [Maven Tile](https://github.com/repaint-io/maven-tiles) that provides the basis for rapid application development of any ACS Share module, extension, service, or web application.  It is intended to be used by other BeeDK components and not directly by any development project.

## Usage

To use this plugin, just reference it with the Maven Tiles plugin as shown in the following snippet.  Do not forget the `extensions` element in the *plugin* configuration.

```xml
<project ...>
	...
	<build>
		...
		<plugins>
			<plugin>
				<groupId>io.repaint.maven</groupId>
				<artifactId>tiles-maven-plugin</artifactId>
				<version>[2.0,3.0)</version>
				<extensions>true</extensions>
				<configuration>
					<tiles>
						...
						<!-- Documentation: https://bitbucket.org/inteligr8/ootbee-beedk/src/stable/beedk-acs-share-rad-tile -->
						<tile>com.inteligr8.ootbee:beedk-acs-share-rad-tile:[1.0.0,2.0.0)</tile>
					</tiles>
				</configuration>
			</plugin>
			...
		</plugins>
		...
	</build>
	...
</project>
```

See the [BeeDK documentation](/inteligr8/ootbee-beedk) on instructions for how to start and stop RAD capabilities.

## Configuration

In your Maven project, set the following properties to define the behavior of this Maven Tile.  Unless otherwise stated, they can only be overridden in the project POM or other Maven Tiles loaded earlier than this Maven Tile.

### Public API

The following properties are intended to be exposed by inheriting Public API Maven Tiles.

| Maven Property             | Required | Default         | Description |
| -------------------------- |:--------:| --------------- | ----------- |
| `acs-share.hotswap.enabled` |         | true            | Enable the HotSwap Agent for live classpath reloading. |
| `acs-share.hotswap.disablePlugins` |  |                 | Do not enable the HotSwap Agent with the following plugins.  All plugins are documented here: https://github.com/HotswapProjects/HotswapAgent/tree/master/plugin |
| `acs-share.debugger.enabled` |        | true            | Enable the JDWP debugger. |
| `acs-share.port`           |          | 8180            | The port to expose on `localhost` for the developer; not for other applications or users. |
| `acs-share.debugger.port`  |          | 8100            | The port to expose on `localhost` for the developer; not for other applications or users. |
| `acs-share.tomcat.opts`    |          |                 | Additional `CATALINA_OPTS` to add to the Apache Tomcat startup.<br/>*May be overridden by any POM parent or Maven Tile.* |
| `acs-share.timeout`        |          | 30000           | The time to wait for the startup to complete, in milliseconds. |
| `tomcat-rad.version`       |          | *not important* | The version of the [Apache Tomcat Rapid Application Development Docker container](/inteligr8/tomcat-rad-docker). |

### Private API

The following properties are only intended to be defined by BeeDK components.

| Maven Property                       | Required | Default            | Description |
| ------------------------------------ |:--------:| ------------------ | ----------- |
| `beedk.deploy.share.warFile`         | **Yes**  |                    | The WAR file to deploy in the Apache Tomcat instance. |
| `beedk.deploy.share.classesDirectory` |         |                    | An additional classpath directory to sideload into the web application. |
| `beedk.deploy.share.testClassesDirectory` |     |                    | An additional test classpath directory to sideload into the web application. |
| `beedk.deploy.share.extDirectory`    |          |                    | An additional directory of JARs to sideload into the web application. |
| `beedk.deploy.share.webDirectory`    |          |                    | An additional directory of web resources to sideload in the web application. |
| `beedk.deploy.share.warDirectory`    |  | `${project.build.warDirectory}` | A build directory for WAR files. |

### Other APIs

Additional less important configurations are inherited from the following Maven Tiles.

*   [`beedk-run-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-run-tile)

## Results

The ACS Share will be started during the `process-classes` Maven phase.
