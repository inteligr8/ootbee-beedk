# BeeDK ACS Share Integration Test Maven Tile for Share Projects

This is a component within the proposed [BeeDK](/inteligr8/ootbee-beedk).  It is considered to be part of the **Private API** of the BeeDK.

## Purpose

This project creates a [Maven Tile](https://github.com/repaint-io/maven-tiles) that provides the basis for integration testing any ACS Share module, extension, service, or web application.

## Usage

To use this plugin, just reference it with the Maven Tiles plugin as shown in the following snippet.  Do not forget the `extensions` element in the *plugin* configuration.

```xml
<project ...>
	...
	<build>
		...
		<plugins>
			<plugin>
				<groupId>io.repaint.maven</groupId>
				<artifactId>tiles-maven-plugin</artifactId>
				<version>[2.0,3.0)</version>
				<extensions>true</extensions>
				<configuration>
					<tiles>
						...
						<!-- Documentation: https://bitbucket.org/inteligr8/ootbee-beedk/src/stable/beedk-acs-share-self-it-tile -->
						<tile>com.inteligr8.ootbee:beedk-acs-share-self-it-tile:[1.0.0,2.0.0)</tile>
					</tiles>
				</configuration>
			</plugin>
			...
		</plugins>
		...
	</build>
	...
</project>
```

## Configuration

In your Maven project, set the following properties to define the behavior of this Maven Tile.  Unless otherwise stated, they can only be overridden in the project POM or other Maven Tiles loaded earlier than this Maven Tile.

### Public API

The following properties are intended to be exposed by inheriting Public API Maven Tiles.

| Maven Property                     | Required | Default            | Description |
| ---------------------------------- |:--------:| ------------------ | ----------- |
| `alfresco.sdk.version`             | **YES**  |                    | For a list of versions and more details, see the [Alfresco SDK](https://github.com/Alfresco/alfresco-sdk).<br/>*Inherited from [`beedk-acs-webapp-artifact-it-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-webapp-artifact-it-tile)* |
| `alfresco.share.version`           | *Maybe*  |                    | The Alfresco Share version to use for integration testing.  See below for more details. |
| `alfresco.war.artifactId`          |          | `share`            | The Maven `artifactId` of a Maven `war` Type Project defined in a Maven Repository.<br/>*Inherited from [`beedk-acs-webapp-artifact-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-webapp-artifact-tile)* |
| `alfresco.war.version`             | *Maybe*  | *See Below*        | The Maven `version` of a Maven `war` Type Project defined in a Maven Repository.<br/>*Inherited from [`beedk-acs-webapp-artifact-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-webapp-artifact-tile)* |
| `project.build.shareExtDirectory`  |  | `${project.build.directory}/ext/share` | An additional directory of JARs to add to the ACS Share at runtime. |
| `acs-platform.url`                 |          | *See Below*        | The URL of the backend ACS Platform to be used. |
| `acs-share.port`                   |          | 8180               | The port to expose on `localhost` for scripts to use during integration testing to verify the state of the application; not for a developer or user. |
| `acs-share.timeout`                |          | 45000              | The time to wait for the startup to complete, in milliseconds. |
| `tomcat-rad.version`               |          | *not important*    | The version of the [Apache Tomcat Rapid Application Development Docker container](/inteligr8/tomcat-rad-docker). |

*   The `alfresco.share.version` is used to default the `alfresco.war.version`.  It is expected to be used by other Maven Tiles, so it is here for convenience.  You must specify either property for this tile to function.
*   The `acs-platform.url` will default to the appropriate value when using the `beedk-acs-platform-*-it-tile` tiles.  This should be overridden if you are not using those tiles and are instead using some other ACS Platform instance.

### Other APIs

Additional less important configurations are inherited from the following Maven Tiles.

*   [`beedk-run-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-run-tile)
*   [`beedk-acs-webapp-artifact-it-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-webapp-artifact-it-tile)

## Results

ACS Share will be started and stopped during the `pre-integration-test` and `post-integration-test` Maven phases, respectively.
