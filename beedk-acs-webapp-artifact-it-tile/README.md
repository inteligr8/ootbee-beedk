# BeeDK ACS Web Application Builder Maven Tile for Integration Tests

This is a component within the proposed [BeeDK](/inteligr8/ootbee-beedk).  It is considered to be part of the **Private API** of the BeeDK.

## Purpose

This project creates a [Maven Tile](https://github.com/repaint-io/maven-tiles) that provides the basis for building Alfresco web applications for integration testing purposes.

## Usage

To use this plugin, just reference it with the Maven Tiles plugin as shown in the following snippet.  Do not forget the `extensions` element in the *plugin* configuration.

```xml
<project ...>
	...
	<build>
		...
		<plugins>
			<plugin>
				<groupId>io.repaint.maven</groupId>
				<artifactId>tiles-maven-plugin</artifactId>
				<version>[2.0,3.0)</version>
				<extensions>true</extensions>
				<configuration>
					<tiles>
						...
						<!-- Documentation: https://bitbucket.org/inteligr8/ootbee-beedk/src/stable/beedk-acs-webapp-artifact-it-tile -->
						<tile>com.inteligr8.ootbee:beedk-acs-webapp-artifact-it-tile:[1.0.0,2.0.0)</tile>
					</tiles>
				</configuration>
			</plugin>
			...
		</plugins>
		...
	</build>
	...
</project>
```

## Configuration

In your Maven project, set the following properties to define the behavior of this Maven Tile.  Unless otherwise stated, they can only be overridden in the project POM or other Maven Tiles loaded earlier than this Maven Tile.

### Public API

The following properties are intended to be exposed by inheriting Public API Maven Tiles.

| Maven Property               | Required | Default           | Description |
| ---------------------------- |:--------:| ----------------- | ----------- |
| `alfresco.sdk.version`       | **YES**  |                   | For a list of versions and more details, see the [Alfresco SDK](https://github.com/Alfresco/alfresco-sdk).  This is used for AMP packaging functionality. |
| `alfresco.war.groupId`       |          | `org.alfresco`    | The Maven `groupId` of a Maven `war` Type Project defined in a Maven Repository.<br/>*Inherited from [`beedk-acs-webapp-artifact-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-webapp-artifact-tile)* |
| `alfresco.war.artifactId`    | **YES**  |                   | The Maven `artifactId` of a Maven `war` Type Project defined in a Maven Repository.<br/>*Inherited from [`beedk-acs-webapp-artifact-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-webapp-artifact-tile)* |
| `alfresco.war.version`       | **YES**  |                   | The Maven `version` of a Maven `war` Type Project defined in a Maven Repository.<br/>*Inherited from [`beedk-acs-webapp-artifact-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-webapp-artifact-tile)* |
| `project.build.ampDirectory` |          | `${project.build.directory}/amp` | This is where AMPs are temporarily copied during the build process. |

### Other APIs

Additional less important configurations are inherited from the following Maven Tiles.

*   [`beedk-acs-webapp-artifact-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-webapp-artifact-tile)

## Results

The configured WAR will downloaded to your local Maven Repository and copied into the configured directory during the configured Maven phase.  The project's AMP `compile`, `runtime`,and `test` dependencies will be downloaded to your local Maven Repository and copied into the configured directory during the `pre-integration-test` Maven phase.  The AMPs will be installed into the WAR during that same `pre-integration-test` Maven phase.  The downloaded and copied AMPs will remain in place.
