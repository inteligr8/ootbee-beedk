# BeeDK Abstract ACS Web Application Maven Tile

This is a component within the proposed [BeeDK](/inteligr8/ootbee-beedk).  It is considered to be part of the **Private API** of the BeeDK.

## Purpose

This project creates a [Maven Tile](https://github.com/repaint-io/maven-tiles) that provides the basis for building Alfresco web applications.

## Usage

To use this plugin, just reference it with the Maven Tiles plugin as shown in the following snippet.  Do not forget the `extensions` element in the *plugin* configuration.

```xml
<project ...>
	...
	<build>
		...
		<plugins>
			<plugin>
				<groupId>io.repaint.maven</groupId>
				<artifactId>tiles-maven-plugin</artifactId>
				<version>[2.0,3.0)</version>
				<extensions>true</extensions>
				<configuration>
					<tiles>
						...
						<!-- Documentation: https://bitbucket.org/inteligr8/ootbee-beedk/src/stable/beedk-acs-webapp-artifact-tile -->
						<tile>com.inteligr8.ootbee:beedk-acs-webapp-artifact-tile:[1.0.0,2.0.0)</tile>
					</tiles>
				</configuration>
			</plugin>
			...
		</plugins>
		...
	</build>
	...
</project>
```

## Configuration

In your Maven project, set the following properties to define the behavior of this Maven Tile.  Unless otherwise stated, they can only be overridden in the project POM or other Maven Tiles loaded earlier than this Maven Tile.

### Public API

The following properties are intended to be exposed by inheriting Public API Maven Tiles.

| Maven Property               | Required | Default           | Description |
| ---------------------------- |:--------:| ----------------- | ----------- |
| `alfresco.war.groupId`       |          | `org.alfresco`    | The Maven `groupId` of a Maven `war` Type Project defined in a Maven Repository. |
| `alfresco.war.artifactId`    | **YES**  |                   | The Maven `artifactId` of a Maven `war` Type Project defined in a Maven Repository. |
| `alfresco.war.version`       | **YES**  |                   | The Maven `version` of a Maven `war` Type Project defined in a Maven Repository. |
| `project.build.warDirectory` |   | `${project.build.directory}/war` | This is where WARs are temporarily copied during the build process. |

### Private API

The following properties are only intended to be defined by BeeDK components.

| Maven Property               | Required | Default           | Description |
| ---------------------------- |:--------:| ----------------- | ----------- |
| `beedk.download.war.phase`   |          | `prepare-package` | The Maven `phase` of when the WAR should be downloaded, copied, or otherwise prepared for packaging.  If packaging for integration testing, you will want this to be `pre-integration-test`. |
| `beedk.download.war.skip`    |          | `true`            | Skip the download of the WAR; only enabled when WAR will be used. |

## Results

The configured WAR will be downloaded to your local Maven Repository and copied into the configured directory during the configured Maven phase.

This tile will result in the definition of the following Maven properties during the respective Maven phase.

| Property                     | Data Type | Phase        | Description |
| ---------------------------- |:---------:|:------------:| ----------- |
| `project.build.warFile`      | Text      | *At startup* | The WAR path and filename for your convenience: `${project.build.warDirectory}/${alfresco.war.artifactId}-${alfresco.war.version}.war` |
