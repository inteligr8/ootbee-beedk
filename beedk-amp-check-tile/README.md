# BeeDK Alfresco Module Package Checker Maven Tile

This is a component within the proposed [BeeDK](/inteligr8/ootbee-beedk).  It is considered to be part of the **Private API** of the BeeDK.

## Purpose

This project creates a [Maven Tile](https://github.com/repaint-io/maven-tiles) that provides the basis for building Alfresco web applications.  It is intended to be used by other BeeDK components and not directly by any development project.

## Usage

To use this plugin, just reference it with the Maven Tiles plugin as shown in the following snippet.  Do not forget the `extensions` element in the *plugin* configuration.

```xml
<project ...>
	...
	<build>
		...
		<plugins>
			<plugin>
				<groupId>io.repaint.maven</groupId>
				<artifactId>tiles-maven-plugin</artifactId>
				<version>[2.0,3.0)</version>
				<extensions>true</extensions>
				<configuration>
					<tiles>
						...
						<!-- Documentation: https://bitbucket.org/inteligr8/ootbee-beedk/src/stable/beedk-amp-check-tile -->
						<tile>com.inteligr8.ootbee:beedk-amp-check-tile:[1.0.0,2.0.0)</tile>
					</tiles>
				</configuration>
			</plugin>
			...
		</plugins>
		...
	</build>
	...
</project>
```

## Configuration

There is nothing to configure for this Maven tile.

## Results

This tile will result in the definition of the following Maven properties during the respective Maven phase.

| Property                     | Data Type | Phase        | Description |
| ---------------------------- |:---------:|:------------:| ----------- |
| `beedk.amp.notest.empty`     | Boolean   | `initialize` | `false` if there any `compile` or `runtime` AMP dependencies on the project; `true` if there are none |
| `beedk.jar.notest.empty`     | Boolean   | `initialize` | `false` if there any `compile` or `runtime` JAR dependencies on the project; `true` if there are none |
| `beedk.amp.test.empty`       | Boolean   | `initialize` | `false` if there any `compile`, `runtime`, or `test` AMP dependencies on the project; `true` if there are none |
| `beedk.jar.test.empty`       | Boolean   | `initialize` | `false` if there any `compile`, `runtime`, or `test` JAR dependencies on the project;`true` if there are none |
| `beedk.amp.notest.classpath` | Text      | `initialize` | Java command line classpath text of all `compile` and `runtime` AMP dependencies on the project |
| `beedk.jar.notest.classpath` | Text      | `initialize` | Java command line classpath text of all `compile` and `runtime` JAR dependencies on the project |
| `beedk.amp.test.classpath`   | Text      | `initialize` | Java command line classpath text of all `compile`, `runtime`, or `test` AMP dependencies on the project |
| `beedk.jar.test.classpath`   | Text      | `initialize` | Java command line classpath text of all `compile`, `runtime`, or `test` JAR dependencies on the project |
