package ${package};

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.activiti.api.boot.BootstrapConfigurer;

@Component
public class BootstrappingService implements BootstrapConfigurer {
    
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired(required = false)
    private List<Bootstrappable> bootstrappables;
    
    @Override
    public void applicationContextInitialized(ApplicationContext applicationContext) {
        if (this.bootstrappables != null) {
            for (Bootstrappable bootstrappable : this.bootstrappables) {
                this.logger.debug("Bootstrapping: " + bootstrappable.getClass());
                bootstrappable.onBootstrap();
            }
        }
    }

}
