package ${package};

import org.activiti.engine.ProcessEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ExampleExtension implements Bootstrappable {
    
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private ProcessEngine services;
    
    @Value("${example.config:true}")
    private boolean exampleBoolean;
    
    @Value("${example.config:#{null}}")
    private String exampleString;
    
    @Override
    public void onBootstrap() {
        this.logger.trace("onBootstrap()");
        
        // TODO do some stuff

        this.logger.info("Model Share Extension initialized");
    }

}
