/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ${package};

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * This class is an example of how you can implement an Activiti Service Task.
 * You can reference it in your BPMN with:
 *    <serviceTask id="serviceTask" activiti:delegateExpression="${exampleServiceTask}" />
 *    <serviceTask id="serviceTask" activiti:expression="${exampleServiceTask.exampleMethod('example parameter')}" />
 */
@Component("exampleServiceTask")
public class ExampleServiceTask implements JavaDelegate {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public void execute(DelegateExecution execution) throws Exception {
		if (this.logger.isTraceEnabled())
			this.logger.trace("execute('" + execution.getId() + "')");
		
		// TODO do some stuff
	}
	
	public void exampleMethod(String param) throws Exception {
		// another example
	}

}
