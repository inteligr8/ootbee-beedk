# BeeDK APS Extension Maven Tile for Integration Tests

This is a component within the proposed [BeeDK](/inteligr8/ootbee-beedk).  It is considered to be part of the **Public API** of the BeeDK.

## Purpose

This project creates a [Maven Tile](https://github.com/repaint-io/maven-tiles) that provides the basis for integration testing any APS extension.

## Usage

To use this plugin, just reference it with the Maven Tiles plugin as shown in the following snippet.  Do not forget the `extensions` element in the *plugin* configuration.

```xml
<project ...>
	...
	<build>
		...
		<plugins>
			<plugin>
				<groupId>io.repaint.maven</groupId>
				<artifactId>tiles-maven-plugin</artifactId>
				<version>[2.0,3.0)</version>
				<extensions>true</extensions>
				<configuration>
					<tiles>
						...
						<!-- Documentation: https://bitbucket.org/inteligr8/ootbee-beedk/src/stable/beedk-aps-ext-it-tile -->
						<tile>com.inteligr8.ootbee:beedk-aps-ext-it-tile:[1.1.0,2.0.0)</tile>
					</tiles>
				</configuration>
			</plugin>
			...
		</plugins>
		...
	</build>
	...
</project>
```

## Configuration

In your Maven project, set the following properties to define the behavior of this Maven Tile.  Unless otherwise stated, they can only be overridden in the project POM or other Maven Tiles loaded earlier than this Maven Tile.

### Public API

The following properties are intended to be exposed by inheriting Public API Maven Tiles.

| Maven Property             | Required | Default         | Description |
| -------------------------- |:--------:| --------------- | ----------- |
| `aps.war.groupId`          |          | `org.alfresco`  | |
| `aps.war.artifactId`       |          | `activiti-app`  | |
| `aps.war.version`          | **Yes**  |                 | |
| `aps.port`                 |          | 8080            | The port to expose on `localhost` for the developer; not for other applications or users. |
| `aps-postgres.port`        |          | 5432            | The port to expose on `localhost` for the developer; not for other applications or users. |
| `aps.debugger.port`        |          | 8000            | The port to expose on `localhost` for the developer; not for other applications or users. |
| `aps.tomcat.opts`          |          |                 | Additional `CATALINA_OPTS` to add to the Apache Tomcat startup.<br/>*May be overridden by any POM parent or Maven Tile.* |
| `aps.timeout`              |          | 120000          | The time to wait for the startup to complete, in milliseconds. |
| `alfresco.license.directory` |  | `${user.home}/alfresco/license` | The base path to search for Alfresco licenses. |
| `aps.license.directory`    |  | `${alfresco.license.directory}/aps` | The base path to search for an APS license. |
| `tomcat-rad.version`       |          | *not important* | The version of the [Apache Tomcat Rapid Application Development Docker container](/inteligr8/tomcat-rad-docker). |
| `aps-postgres.version`     |          | *not important* | The version of PostgreSQL to use in the integration testing infrastructure. |

### Private API

The following properties are only intended to be defined by BeeDK components.

| Maven Property                       | Required | Default            | Description |
| ------------------------------------ |:--------:| ------------------ | ----------- |
| `beedk.deploy.aps.warFile`           | **Yes**  |                    | The WAR file to deploy in the Apache Tomcat instance. |
| `beedk.deploy.aps.classesDirectory`  |          |                    | An additional classpath directory to sideload into the web application. |
| `beedk.deploy.aps.testClassesDirectory` |       |                    | An additional test classpath directory to sideload into the web application. |
| `beedk.deploy.aps.extDirectory`      |          |                    | An additional directory of JARs to sideload into the web application. |
| `beedk.deploy.aps.warDirectory`      |  | `${project.build.warDirectory}` | A build directory for WAR files. |
| `beedk.deploy.aps.dataDirectory`     |  | `${project.build.directory}/alf_data`  | The temporary binary storage location for ACS. |

### Other APIs

Additional less important configurations are inherited from the following Maven Tiles.

*   [`beedk-run-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-run-tile)

## Results

The APS app and database will be started and stopped during the `pre-integration-test` and `post-integration-test` Maven phases, respectively.
