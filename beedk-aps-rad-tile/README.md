# BeeDK APS Application Maven Tile for RAD

This is a component within the proposed [BeeDK](/inteligr8/ootbee-beedk).  It is considered to be part of the **Public API** of the BeeDK.

## Purpose

This project creates a [Maven Tile](https://github.com/repaint-io/maven-tiles) that provides the basis for rapid application development of any application integrating with APS.

## Usage

To use this plugin, just reference it with the Maven Tiles plugin as shown in the following snippet.  Do not forget the `extensions` element in the *plugin* configuration.

```xml
<project ...>
	...
	<build>
		...
		<plugins>
			<plugin>
				<groupId>io.repaint.maven</groupId>
				<artifactId>tiles-maven-plugin</artifactId>
				<version>[2.0,3.0)</version>
				<extensions>true</extensions>
				<configuration>
					<tiles>
						...
						<!-- Documentation: https://bitbucket.org/inteligr8/ootbee-beedk/src/stable/beedk-aps-rad-tile -->
						<tile>com.inteligr8.ootbee:beedk-aps-rad-tile:[1.0.0,2.0.0)</tile>
					</tiles>
				</configuration>
			</plugin>
			...
		</plugins>
		...
	</build>
	...
</project>
```

See the [BeeDK documentation](/inteligr8/ootbee-beedk) on instructions for how to start and stop RAD capabilities.

## Configuration

In your Maven project, set the following properties to define the behavior of this Maven Tile.  Unless otherwise stated, they can only be overridden in the project POM or other Maven Tiles loaded earlier than this Maven Tile.

### Public API

The following properties are intended to be exposed by inheriting Public API Maven Tiles.

| Maven Property             | Required | Default         | Description |
| -------------------------- |:--------:| --------------- | ----------- |
| `aps.port`                 |          | 8080            | The port to expose on `localhost` for the developer; not for other applications or users. |
| `aps.timeout`              |          | 60000           | The time to wait for the startup to complete, in milliseconds. |
| `alfresco.license.directory` |  | `${user.home}/alfresco/license` | The base path to search for Alfresco licenses. |
| `aps.license.directory`    |  | `${alfresco.license.directory}/aps` | The base path to search for an APS license. |
| `aps.docker-tag`           |  | `alfresco/process-services` | The Docker image tag for APS. |
| `aps.version`              |          | 2.4.6           | The version of the [Process Services Docker container](https://hub.docker.com/r/alfresco/process-services). |
| `aps-postgres.version`     |          | *not important* | The version of PostgreSQL to use in the integration testing infrastructure. |

### Other APIs

Additional less important configurations are inherited from the following Maven Tiles.

*   [`beedk-run-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-run-tile)

## Results

The APS Application and database components will be started during the `process-classes` Maven phase.
