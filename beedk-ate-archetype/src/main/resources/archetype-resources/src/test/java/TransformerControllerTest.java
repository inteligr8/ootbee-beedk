package ${package};

import org.alfresco.transformer.AbstractTransformerController;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = TransformerController.class)
public class TransformerControllerTest {

	@Autowired
	protected AbstractTransformerController controller;
	
	@Test @Ignore
	public void test() {
		
	}
}
