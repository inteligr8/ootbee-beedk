# BeeDK Alfresco Transform Engine RAD Maven Tile for non T-Engine Projects

This is a component within the proposed [BeeDK](/inteligr8/ootbee-beedk).  It is considered to be part of the **Public API** of the BeeDK.

## Purpose

This project creates a [Maven Tile](https://github.com/repaint-io/maven-tiles) that provides a Alfresco Transform Engine for the rapid application development of any ATS or ACS module, extension, service, or web application.

> This does not currently support use with ATS.  Only the ALTS (ACS Platform Local Transform Service).

## Usage

To use this plugin, just reference it with the Maven Tiles plugin as shown in the following snippet.  Do not forget the `extensions` element in the *plugin* configuration.

```xml
<project ...>
	...
	<build>
		...
		<plugins>
			<plugin>
				<groupId>io.repaint.maven</groupId>
				<artifactId>tiles-maven-plugin</artifactId>
				<version>[2.0,3.0)</version>
				<extensions>true</extensions>
				<configuration>
					<tiles>
						...
						<tile>com.inteligr8.ootbee:beedk-acs-platform-<!-- See Below -->-rad-tile:[1.0.0,2.0.0)</tile>
						<tile>com.inteligr8.ootbee:beedk-acs-alts-rad-tile:[1.0.0,2.0.0)</tile>
						<!-- Documentation: https://bitbucket.org/inteligr8/ootbee-beedk/src/stable/beedk-ate-sibling-rad-tile -->
						<tile>com.inteligr8.ootbee:beedk-ate-sibling-rad-tile:[1.0.0,2.0.0)</tile>
						...
					</tiles>
				</configuration>
			</plugin>
			...
		</plugins>
		...
	</build>
	...
</project>
```

This tile is meant to be used in conjunction with the `beedk-acs-platform-rad-tile` tile which is automatically included in each of the following BeeDK Public API Maven Tiles.

*   [`beedk-acs-platform-self-rad-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-self-rad-tile)
*   [`beedk-acs-platform-artifact-rad-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-artifact-rad-tile)
*   [`beedk-acs-platform-sibling-rad-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-acs-platform-sibling-rad-tile)

> This Maven Tile should always be declared **AFTER** the Maven Tile used from the list above.

See the [BeeDK documentation](/inteligr8/ootbee-beedk) on instructions for how to start and stop RAD capabilities.

## Configuration

In your Maven project, set the following properties to define the behavior of this Maven Tile.  Unless otherwise stated, they can only be overridden in the project POM or other Maven Tiles loaded earlier than this Maven Tile.

### Public API

The following properties are intended to be exposed by inheriting Public API Maven Tiles.

| Maven Property            | Required | Default         | Description |
| ------------------------- |:--------:| --------------- | ----------- |
| `ate.app.className`       | **Yes**  |                 | The Java class name of the @SpringBootApplication annotated class. |
| `ate.docker.image.name`   | **Yes**  |                 | The Docker image name of the ATE, including the Docker registry host if applicable. |
| `ate.docker.image.tag`    |          | `latest`        | The Docker image tag (version) of the ATE. |
| `ate.port`                |          | 8080            | The port to expose on `localhost` for the developer; not for other applications. |
| `ate.timeout`             |          | 20000           | The time to wait for the startup to complete, in milliseconds. |

### Other APIs

Additional less important configurations are inherited from the following Maven Tiles.

*   [`beedk-run-tile`](/inteligr8/ootbee-beedk/src/stable/beedk-run-tile)

## Results

The configured Alfresco Transform Engine will be started during the `process-classes` Maven phase.  It will **not** be configured automatically for use by the ACS Platform or ATS.
