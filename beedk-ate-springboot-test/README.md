# BeeDK Spring Boot Alfresco Transform Engine Maven Composite POM for Unit Testing

This is a component within the proposed [BeeDK](/inteligr8/ootbee-beedk).  It is considered to be part of the **Public API** of the BeeDK.

## Purpose

This project creates a Maven Composite POM that prorvides the basis for unit testing any Maven project using the [`beedk-ate-springboot`](../beedk-ate-springboot) Composite POM.

## Usage

To use this plugin, just reference it with the standard Maven dependencies as shown in the following snippet.

```xml
<project ...>
	...
	<dependencies>
		...
		<dependency>
			<groupId>com.inteligr8.ootbee</groupId>
			<artifactId>beedk-ate-springboot</artifactId>
			<version>[1.0.0,1.1.0)</version>
			<type>pom</type>
		</dependency>
		<dependency>
			<groupId>com.inteligr8.ootbee</groupId>
			<artifactId>beedk-ate-springboot-test</artifactId>
			<version>[1.0.0,1.1.0)</version>
			<type>pom</type>
			<scope>test</scope>
		</dependency>
		...
	</dependencies>
	...
</project>
```

## Configuration

In your Maven project, set the following properties to define the behavior of this Maven Composite POM.

### Public API

The following properties are exposed by this Maven Composite POM.

| Maven Property                    | Required | Default         | Description |
| --------------------------------- |:--------:| --------------- | ----------- |
| `alfresco.transform-base.version` |          | *not important* | For a list of versions and more details, see the [Alfresco Transform project](https://github.com/Alfresco/alfresco-transform-core). |
| `spring-boot.version`             |          | *not important* | For a list of versions and more details, see the [Spring Boot Project](https://spring.io/projects/spring-boot).<br/>You could use a different Spring Boot version for building your application.  This one is only for test execution. |

## Results

The Spring Boot project will be have all the dependencies required for unit testing an Alfresco Transform Engine.
