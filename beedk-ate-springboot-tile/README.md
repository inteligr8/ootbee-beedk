# BeeDK Spring Boot Alfresco Transform Engine Maven Tile

This is a component within the proposed [BeeDK](/inteligr8/ootbee-beedk).  It is considered to be part of the **Public API** of the BeeDK.

## Purpose

This project creates a [Maven Tile](https://github.com/repaint-io/maven-tiles) that provides the basis for any ATE implemented using the Alfresco Transform Base, which is based on Spring Boot.

## Usage

To use this plugin, just reference it with the Maven Tiles plugin as shown in the following snippet.  Do not forget the `extensions` element in the *plugin* configuration.

```xml
<project ...>
	...
	<build>
		...
		<plugins>
			<plugin>
				<groupId>io.repaint.maven</groupId>
				<artifactId>tiles-maven-plugin</artifactId>
				<version>[2.0,3.0)</version>
				<extensions>true</extensions>
				<configuration>
					<tiles>
						...
						<!-- Documentation: https://bitbucket.org/inteligr8/ootbee-beedk/src/stable/beedk-ate-springboot-tile -->
						<tile>com.inteligr8.ootbee:beedk-ate-springboot-tile:[1.0.0,2.0.0)</tile>
						...
					</tiles>
				</configuration>
			</plugin>
			...
		</plugins>
		...
	</build>
	...
</project>
```

## Configuration

In your Maven project, set the following properties to define the behavior of this Maven Tile.  Unless otherwise stated, they can only be overridden in the project POM or other Maven Tiles loaded earlier than this Maven Tile.

### Public API

The following properties are intended to be exposed by inheriting Public API Maven Tiles.

| Maven Property        | Required | Default | Description |
| --------------------- |:--------:| ------- | ----------- |
| `spring-boot.version` |          | 2.4.3   | For a list of versions and more details, see the [Spring Boot Project](https://spring.io/projects/spring-boot).<br/>You could use a different Spring Boot version for building your application.  This one is only for repackaging it with all dependencies. |

## Results

The Spring Boot project will be repackaged during the `package` phase so all dependencies are consolidated into a single JAR file.

## Initialization

This tile comes with the ability to initialize your project with the required file(s) of a Spring Boot ATE.  You can do this by executing any Maven build with the `scaffold` property.

```sh
mvn -Dscaffold generate-sources
```
