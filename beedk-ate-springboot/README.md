# BeeDK Spring Boot Alfresco Transform Engine Maven Composite POM

This is a component within the proposed [BeeDK](/inteligr8/ootbee-beedk).  It is considered to be part of the **Public API** of the BeeDK.

## Purpose

This project creates a Maven Composite POM that provides the basis for any ATE implemented using the Alfresco Transform Base, which is based on Spring Boot.  This includes libraries that cover ACS integration through both the Alfresco Local Transform Service (HTTP) and the Alfresco Transform Service (MQ & Shared File Store).

## Usage

To use this plugin, just reference it with the standard Maven dependencies as shown in the following snippet.

```xml
<project ...>
	...
	<dependencies>
		...
		<dependency>
			<groupId>com.inteligr8.ootbee</groupId>
			<artifactId>beedk-ate-springboot</artifactId>
			<version>[1.0.0,1.1.0)</version>
			<type>pom</type>
		</dependency>
		...
	</dependencies>
	...
</project>
```

## Configuration

In your Maven project, set the following properties to define the behavior of this Maven Composite POM.

### Public API

The following properties are exposed by this Maven Composite POM.

| Maven Property                    | Required | Default         | Description |
| --------------------------------- |:--------:| --------------- | ----------- |
| `alfresco.transform-base.version` |          | *not important* | For a list of versions and more details, see the [Alfresco Transform project](https://github.com/Alfresco/alfresco-transform-core). |

## Results

The Spring Boot project will be have all the dependencies required for the quick development of an Alfresco Transform Engine.
