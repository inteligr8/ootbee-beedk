# BeeDK Alfresco Local Transform Services Maven Tile for Testing

This is a component within the proposed [BeeDK](/inteligr8/ootbee-beedk).  It is considered to be part of the **Private API** of the BeeDK.

## Purpose

This project creates a [Maven Tile](https://github.com/repaint-io/maven-tiles) that provides Alfresco Local Transform Services for the integration testing of any ACS module, extension, service, or web application.

## Configuration

In your Maven project, set the following properties to define the behavior of this Maven Tile.  Unless otherwise stated, they can only be overridden in the project POM or other Maven Tiles loaded earlier than this Maven Tile.

### Public API

The following properties are intended to be exposed by inheriting Public API Maven Tiles.

| Maven Property       | Required | Default         | Description |
| -------------------- |:--------:| --------------- | ----------- |
| `ate-aio.version`    |          | *not important* | The version of the [Alfresco Transform Core Engine Docker Image](https://hub.docker.com/r/alfresco/alfresco-transform-core-aio/tags). |
