# BeeDK ATS Maven Tile for Testing

This is a component within the proposed [BeeDK](/inteligr8/ootbee-beedk).  It is considered to be part of the **Private API** of the BeeDK.

## Purpose

This project creates a [Maven Tile](https://github.com/repaint-io/maven-tiles) that provides Alfresco Transform Services for the rapid application development of any ACS module, extension, service, or web application.

## Configuration

In your Maven project, set the following properties to define the behavior of this Maven Tile.  Unless otherwise stated, they can only be overridden in the project POM or other Maven Tiles loaded earlier than this Maven Tile.

### Public API

The following properties are intended to be exposed by inheriting Public API Maven Tiles.

| Maven Property         | Required | Default         | Description |
| ---------------------- |:--------:| --------------- | ----------- |
| `ats-router.version`   |          | *not important* | The version of the [Alfresco Transform Router Docker Image](https://quay.io/repository/alfresco/alfresco-transform-router?tab=tags). |
| `ats-sfs.version`      |          | *not important* | The version of the [Alfresco Shared File Store Engine Docker Image](https://quay.io/repository/alfresco/alfresco-shared-file-store?tab=tags). |
