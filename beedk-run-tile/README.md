# BeeDK Alfresco Core Run Maven Tile

This is a component within the proposed [BeeDK](/inteligr8/ootbee-beedk).  It is considered to be part of the **Private API** of the BeeDK.

## Purpose

This project creates a [Maven Tile](https://github.com/repaint-io/maven-tiles) that provides the basis for rapid application development and integration testing any Alfresco module, extension, service, or web application.  It is intended to be used by other BeeDK components and not directly by any development project.

## Usage

To use this plugin, just reference it with the Maven Tiles plugin as shown in the following snippet.  Do not forget the `extensions` element in the *plugin* configuration.

```xml
<project ...>
	...
	<build>
		...
		<plugins>
			<plugin>
				<groupId>io.repaint.maven</groupId>
				<artifactId>tiles-maven-plugin</artifactId>
				<version>[2.0,3.0)</version>
				<extensions>true</extensions>
				<configuration>
					<tiles>
						...
						<!-- Documentation: https://bitbucket.org/inteligr8/ootbee-beedk/src/stable/beedk-run-tile -->
						<tile>com.inteligr8.ootbee:beedk-run-tile:[1.0.0,2.0.0)</tile>
					</tiles>
				</configuration>
			</plugin>
			...
		</plugins>
		...
	</build>
	...
</project>
```

## Configuration

In your Maven project, set the following properties to define the behavior of this Maven Tile.

### Public API

There are no properties that are intended to be exposed by inheriting Public API Maven Tiles.

### Private API

The following properties are only intended to be defined by BeeDK components.

| Maven Property                         | Required | Default           | Description |
| -------------------------------------- |:--------:| ----------------- | ----------- |
| `beedk.plugins.fabric8.docker.version` |          | *not important*   | The version of the [docker-maven-plugin](https://dmp.fabric8.io/) Maven Plugin. |

## Results

The Docker features of the BeeDK will be initialized and configured for all subsequent use cases.  These features are used for both integration testing and rapid application development support.
