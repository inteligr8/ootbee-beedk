/*
 * #%L
 * Alfresco Transform Core
 * %%
 * Copyright (C) 2005 - 2019 Alfresco Software Limited
 * %%
 * This file is part of the Alfresco software.
 * -
 * If the software was purchased under a paid Alfresco license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 * -
 * Alfresco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * -
 * Alfresco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * -
 * You should have received a copy of the GNU Lesser General Public License
 * along with Alfresco. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
package ${groupId};

import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Assert;
import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.HttpStatusCodeException;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class HttpRequestIT {

	private final Logger logger = LoggerFactory.getLogger(HttpRequestIT.class);
	
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;
    
    protected String baseUrl;
    
    @PostConstruct
    public void init() {
    	this.baseUrl = "http://localhost:" + this.port;
    }
    
	@Test
	public void testRootPath() {
		String result = this.restTemplate.getForObject(this.baseUrl, String.class);
		if (this.logger.isDebugEnabled())
			this.logger.debug("testRootPath(): result: " + result);
		
		Assert.assertNotNull("A result from the HTTP GET was expected", result);
		Document htmldoc = Jsoup.parse(result);
		Assert.assertNotNull("An HTML compliant result was expected: " + result.substring(0, 50), htmldoc);
		
		Elements elements = htmldoc.select("html body h2");
		Assert.assertFalse("The HTML body is expected to have an h2 element: html: " + htmldoc.toString(), elements.isEmpty());
		Assert.assertEquals("The HTML body is expected to have just one h2 element", 1, elements.size());
		Assert.assertEquals("The HTML body header is not what was expected", "${shortname} Test Transformation", elements.html());
		
		elements = htmldoc.select("html input");
		Set<String> inputs = new HashSet<String>();
		for (Element element : elements)
			inputs.add(element.attr("name"));
		Assert.assertTrue("The HTML is expected to have a form input for 'file': " + inputs.toString(), inputs.contains("file"));
		Assert.assertTrue("The HTML is expected to have a form input for 'targetExtension': " + inputs.toString(), inputs.contains("targetExtension"));
	}
    
	@Test
	public void testLogPath() {
		String result = this.restTemplate.getForObject(this.baseUrl + "/log", String.class);
		if (this.logger.isDebugEnabled())
			this.logger.debug("testLogPath(): result: " + result);
		
		Assert.assertNotNull("A result from the HTTP GET was expected", result);
		Document htmldoc = Jsoup.parse(result);
		Assert.assertNotNull("An HTML compliant result was expected: " + result.substring(0, 50), htmldoc);
		
		Elements elements = htmldoc.select("html body div h2");
		Assert.assertFalse("The HTML is expected to have an html/body/div/h2 element: html: " + htmldoc.select("html").toString(), elements.isEmpty());
		Assert.assertEquals("The HTML is expected to have just one html/body/div/h2 element", 1, elements.size());
		Assert.assertEquals("The HTML body header is not what was expected", "${shortname} Log Entries", elements.html());
	}
    
	@Test
	public void testNoPath() {
		try {
			ResponseEntity<String> response = this.restTemplate.getForEntity(this.baseUrl + "/doesnotexist", String.class);
			Assert.assertEquals("An unexpected path must return a 404 error", 404, response.getStatusCodeValue());
		} catch (HttpStatusCodeException hsce) {
			Assert.assertEquals("An unexpected path must return a 404 error", 404, hsce.getRawStatusCode());
		}
	}
    
	@Test
	public void testServiceGet() {
		try {
			ResponseEntity<String> response = this.restTemplate.getForEntity(this.baseUrl + "/transform", String.class);
			Assert.assertEquals("An unexpected path must return a 405 error", 405, response.getStatusCodeValue());
		} catch (HttpStatusCodeException hsce) {
			Assert.assertEquals("An unexpected path must return a 405 error", 405, hsce.getRawStatusCode());
		}
	}
	
	@Test
	public void testServiceNoFile() {
		try {
			ResponseEntity<String> response = this._testService(null, "trgt");
			Assert.assertEquals("An unexpected path must return a 400 error", 400, response.getStatusCodeValue());
		} catch (HttpStatusCodeException hsce) {
			Assert.assertEquals("An unexpected path must return a 400 error", 400, hsce.getRawStatusCode());
		}
	}
	
	@Test @Ignore
	public void testServiceNoTargetExtension() {
		try {
			ResponseEntity<String> response = this._testService("quick.src", null);
			Assert.assertEquals("An unexpected path must return a 400 error", 400, response.getStatusCodeValue());
		} catch (HttpStatusCodeException hsce) {
			Assert.assertEquals("An unexpected path must return a 400 error", 400, hsce.getRawStatusCode());
		}
	}
	
	@Test @Ignore
	public void testServiceQuick() {
		this._testService("quick.src", "trgt");
	}
    
	protected ResponseEntity<String> _testService(String filename, String targetExtension) {
		LinkedMultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
        if (filename != null)
            parameters.add("file", new ClassPathResource(filename));
        if (targetExtension != null)
        	parameters.add("targetExtension", targetExtension);
        
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<LinkedMultiValueMap<String, Object>> entity = new HttpEntity<>(parameters, headers);
		return this.restTemplate.postForEntity(this.baseUrl + "/transform", entity, String.class);
	}
    
}
