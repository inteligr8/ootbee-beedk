# BeeDK Docker Spring Boot Maven Tile

This is a component within the proposed [BeeDK](/inteligr8/ootbee-beedk).  It is considered to be part of the **Private API** of the BeeDK.

## Purpose

This project creates a [Maven Tile](https://github.com/repaint-io/maven-tiles) that provides the basis for any Spring Boot application that is delivered as a Docker Image.

## Usage

To use this plugin, just reference it with the Maven Tiles plugin as shown in the following snippet.  Do not forget the `extensions` element in the *plugin* configuration.

```xml
<project ...>
	...
	<build>
		...
		<plugins>
			<plugin>
				<groupId>io.repaint.maven</groupId>
				<artifactId>tiles-maven-plugin</artifactId>
				<version>[2.0,3.0)</version>
				<extensions>true</extensions>
				<configuration>
					<tiles>
						...
						<!-- Documentation: https://bitbucket.org/inteligr8/ootbee-beedk/src/stable/beedk-springboot-docker-base-tile -->
						<tile>com.inteligr8.ootbee:beedk-springboot-docker-base-tile:[1.0.0,2.0.0)</tile>
					</tiles>
				</configuration>
			</plugin>
			...
		</plugins>
		...
	</build>
	...
</project>
```

## Configuration

In your Maven project, set the following properties to define the behavior of this Maven Tile.  Unless otherwise stated, they can only be overridden in the project POM or other Maven Tiles loaded earlier than this Maven Tile.

### Public API

The following properties are intended to be exposed by inheriting Public API Maven Tiles.

| Maven Property                 | Required | Default            | Description |
| ------------------------------ |:--------:| ------------------ | ----------- |
| `docker.source.directory`      |          | `src/main/docker`  | The source directory of the `Dockerfile` and related resources. |
| `beedk.deploy.dockerImageOnly` |          | true               | Set to `true` to skip the Maven `deploy` of the Spring Boot application; `false` to deploy it to the configured Maven Repository.  Regardless, the Docker Image will be deployed to the configured Docker Registry. |
| `docker.image.registry`        | **Yes**  |                    | The Docker Registry where to push the built Docker Image.  If none is specified, Docker pushes to the [Docker Hub](https://hub.docker.com). |
| `docker.image.name`            | **Yes**  |                    | The Docker Image name.  You typically want to pattern it as `prefix/${project.artifactId}`. |
| `docker.image.tag`             |          | *Project Version*  | The Docker Image tag or label.  This will default to the Maven project version. |

## Results

A Docker image will be built using your local Docker installation during the `package` phase.  It will be tagged both with and without the configured Docker Registry.  During the `deploy` phase, the Docker image will be pushed to the configured Docker Registry.
